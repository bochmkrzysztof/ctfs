#include <stdlib.h>
#include <stdio.h>

int main() {
    void* a = malloc(0);
    void* b = malloc(-1);
    void* c = malloc(1);

    printf("%p %p %p\n", a, b, c);
}
