---
geometry: margin=2cm
document_class: report
---

# Pwn: Hellhound

## Flag

```
HTB{1t5_5p1r1t_15_5tr0ng3r_th4n_m0d1f1c4t10n5}
```

## Summary 

Application reads data from user, part of it can
be later used as an address of a buffer user can
write to. Application through one of the options
provides us with an address to a variable on stack.
We can simply overwrite the return address.

## Detailed solution

After running the program we can choose one of three
options:

 - Analyze chipset - which gives us stack address
 - Modify hardware - that allows us to write data
    into a buffer
 - Check results - changes buffor's address into 
    one provided by user 
