#!/bin/python

from pwn import *

context.terminal = ['alacritty', '-e', 'sh', '-c']

# python is so ugly, no 'let', 'var' or anything in
# front of var name
filename = './ffi'
gs = '''
    b main
    c
'''

#io = gdb.debug(filename, gdbscript=gs)
#io = remote('localhost', 1337)

file1 = open('a', 'r')
Lines = file1.readlines()

for line in Lines:
    io = process(filename)

    line = line[:-1]
    flag = 'HTB{_fr34ky_' + line + '_}'

    io.sendline(flag)

    print(flag + ': ' + str(io.readline())[:-1])

    io.close()

io.interactive()
