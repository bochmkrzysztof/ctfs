#!/bin/python

from pwn import *

context.terminal = ['alacritty', '-e', 'sh', '-c']

# python is so ugly, no 'let', 'var' or anything in
# front of var name
filename = 'ls'
gs = '''
    b main
    c
'''

io = gdb.debug(filename, gdbscript=gs)
#io = remote('localhost', 1337)

io.interactive()
