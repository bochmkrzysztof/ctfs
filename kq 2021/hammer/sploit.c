#include <stdio.h>
#include <stdint.h>

int main() {
    printf("%lli\n", 0b0010010010010010010010010010010010010010010010010010010010010010);
    for(int64_t i = -1;; i--) {
       if((i * 7 + 1) == -1) {
          printf("%lld", i); 
            return 0;
       }
    }    
}
