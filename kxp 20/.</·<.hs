#!/usr/bin/env runhaskell
{-# LANGUAGE OverloadedStrings #-}
import Prelude hiding (replicate, putStrLn)
import Data.List hiding (replicate)
import Data.Tuple
import Data.Ord
import Data.Function
import Data.ByteString (replicate)
import Data.ByteString.Char8 (putStrLn, pack)
import Control.Monad
import Control.Arrow
import Control.Applicative
import Crypto.Cipher.AES
import Crypto.Hash.SHA256
import Unsafe.Coerce

c = "\xd7\xc8\x35\x14\xc4\x27\xcd\x6f\x78\x3a\x80\x57\x76\xb0\xfd\x42\x25\xe4\x87\x5f\x99\x28\x87\x0a\x06\xef\x63\x81\x44"

main
  = (>>=) getContents $ read >>> putStrLn <<< ( <<< unsafeCoerce . and <<< flip
    `liftM` flip liftM2 `uncurry` (fmap unsafeCoerce $ swap <**> (<>) $ (&) <$>
    [0..39] & pure) <<< ((.) or <<<) . (<$>) (flip (<$>) >>> (.) $ (>>> \[x] ->
    x) . flip elemIndices & \(??) -> (.) . (>>>) . (. flip (??)) . ((.) (>=) .)
    . (>>>) <*> (.) (<**>) . (<*>) . (.) (??) & \t -> [(&) $ id $ fst <$> (flip
    sortBy [0..39] . comparing . (!!) *** drop 40 <<< return . snd ^>> (<>) <*>
    swap) `iterate` pure (tail $ flip iterate 44 $ flip mod 400013 <$> (+) 42 .
    (*) 1337) & tail & (!!)] <*> [(<<^) flip <<< t >>> ((!!) >>>), (.) (==) <$>
    (!!) & return, (>>>) (??) . t <<< (. (+) 40)]) . (>>>) (.) . (.) <<< ( >>>)
    . (&)) . maybe "wrong" <$> (>>>) pure (>>> initAES . hash . pack . show >>>
    decryptCTR `flip` replicate 16 0 `flip` c) <*> id

