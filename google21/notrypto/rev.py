
SBOX = {
    (0, 0): (0, 0),
    (0, 1): (1, 0),
    (0, 2): (0, 1),
    (1, 0): (1, 1),
    (1, 1): (0, 2),
    (1, 2): (1, 2),
}

RSBOX = {
    (0, 0): (0, 0),
    (1, 0): (0, 1),
    (0, 1): (0, 2),
    (1, 1): (1, 0),
    (0, 2): (1, 1),
    (1, 2): (1, 2),
}


def do(l1, l2, d):
    for i in range(len(l1)):
        l1[i], l2[i] = RSBOX[l1[i], l2[i]]

    if(l1[-1] == 0):
        l1.pop()

        l1.insert(0, 0)
        step(l1, l2, d + 1)
        l1.pop(0)

        if(len(l1) > 0): 
            if(l1[0] == 1 and l2[0] == 1):
                l2.pop(0)
                step(l1, l2, d + 1)
                l2.insert(0, 1)

        l1.append(0)

    for i in range(len(l1)):
        l1[i], l2[i] = SBOX[l1[i], l2[i]]


def step(l1, l2, d):
    if(d > 30 and len(l1) <= 24):
        print(l1)
        print(l2)
        return
    
    do(l1, l2, d)

    l1.append(0)
    l2.append(0)

    do(l1, l2, d)

    l1.pop()
    l2.pop()

l1 = [1]
l2 = [0]

step(l1, l2, 0)
