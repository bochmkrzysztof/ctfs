#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <assert.h>


int unopt(char* flag) {
	char ROM[256] = {0xbb,0x55,0xab,0xc5,0xb9,0x9d,0xc9,0x69,0xbb,0x37,0xd9,0xcd,0x21,0xb3,0xcf,0xcf,0x9f,0x9,0xb5,0x3d,0xeb,0x7f,0x57,0xa1,0xeb,0x87,0x67,0x23,0x17,0x25,0xd1,0x1b,0x8,0x64,0x64,0x35,0x91,0x64,0xe7,0xa0,0x6,0xaa,0xdd,0x75,0x17,0x9d,0x6d,0x5c,0x5e,0x19,0xfd,0xe9,0xc,0xf9,0xb4,0x83,0x86,0x22,0x42,0x1e,0x57,0xa1,0x28,0x62,0xfa,0x7b,0x1b,0xba,0x1e,0xb4,0xb3,0x58,0xc6,0xf3,0x8c,0x90,0x3b,0xba,0x19,0x6e,0xce,0xdf,0xf1,0x25,0x8d,0x40,0x80,0x70,0xe0,0x4d,0x1c};
	strcpy(ROM + 128, flag);

	uint8_t A, B, C, X, Y, Z, I, M, N, O, P, Q, R;

start:
	goto lab6;

lab1:
	R = ~R;
	Z = 1;
	R = R + Z;
	R = R + Z;
	if(R == 0) goto lab8;

	R = R + Z;
	if(R == 0) goto lab10;

	R = R + Z;
	if(R == 0) goto lab10;

	assert(0);

	return 999;

lab2:
	X = 1;
	Y = 0;

lab3:
	if(X == 0) goto lab5;
	Z = X;
	Z = Z & B;
	if(Z == 0) goto lab4;

	Y = Y + A;

lab4:
	X = X + X;
	A = A + A;
	goto lab3;

lab5:
	A = Y;
	goto lab1;

lab6:
	I = 0;
	M = 0;
	N = 1;
	P = 0;
	Q = 0;

lab7:
	B = 0xe5;
	B = B + I;
	if(B == 0) goto lab9;
	B = 0x80;
	B = B + I;
	A = ROM[B];
	B = ROM[I];
	R = 1;
	goto lab2;

lab8:
	O = M;
	O = O + N;
	M = N;
	N = O;
	A = A + M;
	B = 0x20;
	B = B + I;
	C = ROM[B];
	A = A ^ C;
	P = P + A;
	B = 0x40;
	B = B + I;
	A = ROM[B];
	A = A ^ P;
	Q = Q | A;

	//printf("Q = %hhu, I = %hhu\n", Q, I);
	if(Q) return I;

	A = 1;
	I = I + A;
	goto lab7;

lab9:
	if(Q == 0) goto lab10;
	assert(0);

lab10:
	return 999;
}

int main(int, char** argv) {
#define CHAR_a 97
#define CHAR_b 98
#define CHAR_c 99
#define CHAR_d 100
#define CHAR_e 101
#define CHAR_f 102
#define CHAR_g 103
#define CHAR_h 104
#define CHAR_i 105
#define CHAR_j 106
#define CHAR_k 107
#define CHAR_l 108
#define CHAR_m 109
#define CHAR_n 110
#define CHAR_o 111
#define CHAR_p 112
#define CHAR_q 113
#define CHAR_r 114
#define CHAR_s 115
#define CHAR_t 116
#define CHAR_u 117
#define CHAR_v 118
#define CHAR_w 119
#define CHAR_x 120
#define CHAR_y 121
#define CHAR_z 122
#define CHAR_A 65
#define CHAR_B 66
#define CHAR_C 67
#define CHAR_D 68
#define CHAR_E 69
#define CHAR_F 70
#define CHAR_G 71
#define CHAR_H 72
#define CHAR_I 73
#define CHAR_J 74
#define CHAR_K 75
#define CHAR_L 76
#define CHAR_M 77
#define CHAR_N 78
#define CHAR_O 79
#define CHAR_P 80
#define CHAR_Q 81
#define CHAR_R 82
#define CHAR_S 83
#define CHAR_T 84
#define CHAR_U 85
#define CHAR_V 86
#define CHAR_W 87
#define CHAR_X 88
#define CHAR_Y 89
#define CHAR_Z 90
#define CHAR_0 48
#define CHAR_1 49
#define CHAR_2 50
#define CHAR_3 51
#define CHAR_4 52
#define CHAR_5 53
#define CHAR_6 54
#define CHAR_7 55
#define CHAR_8 56
#define CHAR_9 57
#define CHAR_LBRACE 123
#define CHAR_RBRACE 125
#define CHAR_UNDERSCORE 95

	char flag[256] = {};
	strcpy(flag, "CTF{");
	int good = 4;

	while(good != 27) {
		for(char i = 97; i <= 122; i++) {
			flag[good] = i;
			if(unopt(flag) > good) {
				good++;
				continue;
			}
		}
		for(char i = 65; i <= 90; i++) {
			flag[good] = i;
			if(unopt(flag) > good) {
				good++;
				continue;
			}
		}
		for(char i = 48; i <= 57; i++) {
			flag[good] = i;
			if(unopt(flag) > good) {
				good++;
				continue;
			}
		}

		flag[good] = 123;
		if(unopt(flag) > good) {
			good++;
			continue;
		}

		flag[good] = 125;
		if(unopt(flag) > good) {
			good++;
			continue;
		}


		flag[good] = 95;
		if(unopt(flag) > good) {
			good++;
			continue;
		}
	}

	puts(flag);
}