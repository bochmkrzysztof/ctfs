#include <stdio.h>

#include <stdint.h>

int main() {
	putchar('{');

	char buffor[16];

	for(int i = 0; i <= 0b01011010; i++) {
		uint8_t var = 0;
		for(int j = 0; j < 8; j++) {
			fgets(buffor, 16, stdin);
			char ch = buffor[0];

			if(ch == 0x31) var |= (1 << j);
		}

		printf("0x%hhx,", var);
	}
	puts("0x00}");
}