#include <stdio.h>

#include <stdint.h>


int main() {
	uint8_t A = 4;
	uint8_t B = 5;
	
	uint8_t Z;

lab2:
	uint8_t X = 1;
	uint8_t Y = 0;

lab3:
	if(X == 0) goto lab5;
	Z = X;
	Z = Z & B;
	if(Z == 0) goto lab4;

	Y = Y + A;

lab4:
	X = X + X;
	A = A + A;
	goto lab3;

lab5:
	A = A | Y;
	
	printf("A = %hhu\nY = %hhu\n", A, Y);
}
