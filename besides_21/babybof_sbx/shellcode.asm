[bits 64]
    mov rax, 1
    mov rdi, 1
    lea rsi, [rel msg]
    mov rdx, 5
    syscall

    mov rax, 63
    lea rdi, [rel buf]
    syscall
    
    mov rax, 1
    mov rdi, 1
    lea rsi, [rel buf]
    mov rdx, 325
    syscall         ; write

    xor rax, rax
    mov al, 60
    xor rdi, rdi  
    syscall

    jmp $

msg:
    db "hello"

dot:
    db '.',0

how:
    dq 0x10000 ; O_DIRECTORY
    dq 0x0
    dq 0x0

buf:
times 512 db 0    
