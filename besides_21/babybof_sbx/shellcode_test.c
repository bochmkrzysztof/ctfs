#include <asm-generic/fcntl.h>
#include <linux/openat2.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <linux/fcntl.h>
#include <linux/fs.h>
#include <sys/syscall.h>

#define BUF_SIZE 512
struct linux_dirent {
   long           d_ino;
   off_t          d_off;
   unsigned short d_reclen;
   char           d_name[];
};

int main() {
    struct open_how how = {
        O_DIRECTORY | O_RDONLY
    };
struct linux_dirent *d;
    int nread, bpos;
    long fd = syscall(SYS_openat2, AT_FDCWD, ".", &how, sizeof(struct open_how));


    char buf[BUF_SIZE];

    for ( ; ; ) {
       nread = syscall(SYS_getdents, fd, buf, BUF_SIZE);


       if (nread == 0)
           break;

       for (bpos = 0; bpos < nread;) {
           d = (struct linux_dirent *) (buf + bpos);
           if (d->d_ino != 0) printf("%s\n", (char *) d->d_name);
           bpos += d->d_reclen;
       }
   }
}
