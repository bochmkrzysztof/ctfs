#define _GNU_SOURCE
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/syscall.h>

int main() {
    struct file_handle *fhp;
    int fd;
    int mount_id;

    fhp = malloc(sizeof(*fhp));

    fhp->handle_bytes = 0;
    name_to_handle_at(AT_FDCWD, "a", fhp, &mount_id, AT_SYMLINK_FOLLOW|AT_EMPTY_PATH);

    fhp = realloc(fhp, sizeof(*fhp) + fhp->handle_bytes);

    name_to_handle_at(AT_FDCWD, "a", fhp, &mount_id, AT_SYMLINK_FOLLOW|AT_EMPTY_PATH);
    
    fd = open_by_handle_at(AT_FDCWD, fhp, O_RDONLY | O_DIRECTORY);
    perror("open_by_handle_at");
}

