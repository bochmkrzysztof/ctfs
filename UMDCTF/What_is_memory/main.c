#include <stdio.h>

unsigned int hash_string(char *string)
{
  unsigned int hash;
  char character;
  
  character = *string;
  hash = 0x1505;
  while (character != 0) {
    string = string + 1;
    hash = hash * 0x21 + (unsigned int)character;
    character = *string;
  }
  return hash;
}

char check_rec(char* buffor, char* flag, int i) {
	buffor[i] = 0;
	
	sprintf(flag, "UMDCTF-{%s}", buffor);
	unsigned int hash = hash_string(flag);

	if(0x57c5a7cd == hash) {
		puts(flag);
		return 1;
	}

	if(i == 4) return 0;

	for(char c = 0x20; c > 0; c++) {
		buffor[i] = c;
		if(check_rec(buffor, flag, i+1))
			return 1;
	}

	return 0;
}

int main() {
	char buffor[256] = {};
	char flag[256];

	return 1- check_rec(buffor, flag, 0);
}

