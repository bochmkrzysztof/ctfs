#include <stdio.h>

#include <vector>

int main() {
	{
		char buffor[1024];
		int o = 24;
		while(o--) fgets(buffor, 1024, stdin);
	}
	std::vector<short> vec;

	getchar();

	short number;
	while(1) {
		scanf("%d", &number);
		vec.push_back(number);
		if(getchar() == ']')
			break;
		getchar();
	}

	struct {
		int sum = -99999999;
		int start = 0;
		int end = 0;
	} biggest;

	struct {
		int sum = 0;
		int start = 0;
	} current;

	current.sum = vec[0];

	for(int i = 1; i < vec.size(); i++) {
		if(vec[i] == vec[i-1] + 1) {
			current.sum += vec[i];
		} else {
			if(biggest.end - biggest.start < i - current.start) {
			//if(biggest.sum < current.sum) {
				biggest.sum = current.sum;
				biggest.start = current.start;
				biggest.end = i - 1;
			}

			current.sum = vec[i];
			current.start = i;
		}
	}

	printf("%d, %d, %d\n", biggest.sum, biggest.start, biggest.end);
}
