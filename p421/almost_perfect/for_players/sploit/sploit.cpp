#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <stdint.h>

#include <vector>
#include <set>
#include <string>

#define wlen 20

bool isPossible(char* key, std::vector<char*>& enc, std::set<std::string>& words) {
	uint32_t len = strlen(key);

	for(char* encw : enc) {
		char buffor[wlen];
		buffor[len] = 0;

		for(uint32_t i = 0; i < len; i++) {
			char a = encw[i] - 'a';
			char b = key[i] - 'a';
			char c = a - b;

			if ( c < 0 ) c += 26;
			
			buffor[i] = c + 'a';			
		}
	

		if(words.count(std::string(buffor)) == 0) 
		{
			return false;
		}}
	return true;
}

int main() {
	std::set<std::string>* words = new std::set<std::string>[wlen];
	std::vector<char*>* encwords = new std::vector<char*>[wlen];

	ssize_t len;

	size_t n = 0;
	char* buffor = nullptr;

	FILE* dict = fopen("dict.txt", "r");

	char key[wlen];

	while((len = getline(&buffor, &n, dict)) >= 0) {
		if(!(len < wlen)) continue;	
		if(len == 0) continue;

		char* str = new char[len + 1];
		strcpy(str, buffor);

		str[len -= 2] = 0; //CRLF :(

		words[len].insert(std::string(str));
	}

	FILE* enc = fopen("enc.txt", "r");
	//FILE* enc = fopen("../output.txt", "r");

	while((len = getdelim(&buffor, &n, ' ', enc)) >= 0) {
		assert(len < wlen);

		if(len == 0) continue;

		char* str = new char[len + 1];
		strcpy(str, buffor);

		str[len--] = 0;

		encwords[len].push_back(str);
	}

	for(uint32_t i = 0; i < wlen; i++)
		printf("%u: %u\n", i, encwords[i].size());

	key[2] = 0;
	for(key[0] = 'a'; key[0] <= 'z'; key[0]++)
		for(key[1] = 'a'; key[1] <= 'z'; key[1]++)
			if(isPossible(key, encwords[2], words[2]))
				puts(key);
}
