from pwn import *

#flag: p4{alternate_title:i_cant_even...375614b1b29713bd}

game = remote('cards.zajebistyc.tf', 17004)
#game = process("for_players/playing_cards")
solver = process("./a.out")

game.recvline()
game.sendline("1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 0 51 0 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 1")

for x in range(0, 52):
    game.recvuntil("are: ")
    numbers = game.recvline()

    print(numbers)
    solver.sendline(numbers)
    
    commands = solver.recvline()
    
    print(commands)
    game.sendline(commands)

log.info(game.recvall())
