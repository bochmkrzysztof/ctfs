#include <stdio.h>
#include <string.h>
#include <stdint.h>

void a(char* deck, int& head, int& tail) {
	char first = deck[0];
	for(uint32_t i = 0; i < 51; i++)
		deck[i] = deck[i+1];
	deck[50] = first;

	head--;
	if(head < 0) head = 50;
	tail--;
	if(tail < 0) tail = 50;

	putchar('a');
}

void b(char* deck, int& head, int& tail) {
	char first = deck[1];
	for(uint32_t i = 1; i < 52; i++)
		deck[i] = deck[i+1];
	deck[51] = first;

	head--;
	if(head < 1) head = 51;
	tail --;
	if(tail < 1) tail = 51;

	putchar('b');
}

void print(char* deck) {
	putchar(10);
	for(uint32_t i = 0; i < 52; i++)
		printf("%hhd ", deck[i]);
	putchar(10);
}

int main() {

	char luckyNumbers[52];
	char deck[52];

	for(uint32_t i = 0; i < 52; i++) 
		deck[i] = i;	

	int n = 52;
	while(n--) {
		for(uint32_t i = 0; i < 52; i++)
			scanf("%hhu", luckyNumbers + i);
		
		int len = 1;
		int head = luckyNumbers[0];
		int tail = head;

		while(memcmp(luckyNumbers, deck, 52)) {
			if(deck[0] == luckyNumbers[len]) {
				while(tail != 50)
					b(deck, head, tail);
				a(deck, head, tail);
				tail = 50;
			}
			else {
				while(deck[51] != luckyNumbers[len])
					b(deck, head, tail);
				while(tail != 50)
					a(deck, head, tail);
				tail = 51;
			}
			
			len++;
		}

		putchar(10);
	}

}
