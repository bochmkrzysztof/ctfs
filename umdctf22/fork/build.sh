#!/bin/sh
docker build -f build.Dockerfile -t fnkbuild .

id="$(docker create fnkbuild)"

docker cp "$id:/root/main" ./fnk
docker container rm "$id"

chown "$(whoami):" ./fnk
