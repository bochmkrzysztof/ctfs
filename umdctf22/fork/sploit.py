#!/bin/python

from pwn import *

e = ELF('./fnk')


context.terminal = ['alacritty', '-e', 'sh', '-c']

io = gdb.debug('./fnk', gdbscript = '''
    b main
    b proc_flask

    set follow-fork-mode child

    c
        ''')

io.interactive()
