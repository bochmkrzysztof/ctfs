import base64

b64 = '{"showpassword":"yes","bgcolor":"#000000"}'

passwd = 'qw8J'
result = ''

print(passwd)

for i in range(len(b64)):
	result += chr(ord(b64[i]) ^ ord(passwd[i % len(passwd)]))

print(base64.b64encode(bytearray(result, 'ascii')))
