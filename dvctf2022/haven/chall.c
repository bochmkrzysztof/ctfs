#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>

void win(void) {
    printf("You're a lucky guy. Welcome to heaven :\n");
    system("cat flag.txt");
}

void init();

int main() {

    init();
    printf("Open the right door a hundred times, to open heaven's gates\n");
    printf("You have 20 seconds or the gates will be closed forever...\n\n");
    printf(
           "  __________  __________  __________ \n"
           " |  __  __  ||  __  __  ||  __  __  |\n"
           " | |  ||  | || |  ||  | || |  ||  | |\n"
           " | | 0||  | || | 1||  | || | 2||  | |\n"
           " | |__||__| || |__||__| || |__||__| |\n"
           " |  __  __()||  __  __()||  __  __()|\n"
           " | |  ||  | || |  ||  | || |  ||  | |\n"
           " | |  ||  | || |  ||  | || |  ||  | |\n"
           " | |__||__| || |__||__| || |__||__| |\n"
           " |__________||__________||__________|\n\n"
           );

    uint32_t base = 1647103701 + 1;
    uint32_t tm = base;
    uint32_t sub = 0;

    int sec = 21 , end = base + sec, right_door, choosed_door;

    for( int i = 1 ; i < 101 ; i++ )
    {
        srand(tm % sec);
        right_door = rand()%3;

	    printf("%d\n", right_door);

        sec = end - tm;


	    sub ++;
	    if(sub == 10) {
	        sub = 0;
	        tm++;
	    }
    }

    win();
}

void init() {
    setvbuf(stdout, 0, 2, 0);
    setvbuf(stdin, 0, 2, 0);
}
