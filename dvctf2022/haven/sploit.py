#!/bin/python

from pwn import *

check = 0
correct = []

p = log.progress("Known doors")

while len(correct) != 100:
    bad = True    

    count = 0

    while bad:
        for j in range(0, 2):
            try:
                bad = False

                log.info('Good: ' + str(len(correct)))
                io = remote('challs.dvc.tf', 7337)

                for k in correct:
                    io.recvuntil(b'...\n')
                    io.sendline(str(k))

                
                io.recvuntil(b'...\n')
                io.sendline(str(j))


                resp = io.recv(3)

                if resp == b'YOU':
                    pass
                else:
                    correct.append(j)
                    break
            except:
                bad = True

        count += 1

        if count == 3:
            correct.pop()

p.success()

io.interactive()

