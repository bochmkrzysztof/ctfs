#include <stdio.h>
#include <stdint.h>

#define LL(i) string[i] >= 0x30 && string[i] < 0x80

int main() {
	uint16_t best = 0x100;
	for(uint64_t i = 0x30000000; i <= 0xffffffff; i++) {
		uint8_t* string = (uint8_t*) &i;

		uint8_t xor = string[0] ^ 'D' + string[1] ^ 'U' + string[2] ^ 'C' + string[3] ^ 'T';
		uint8_t last = 'F' ^ (0x100 - xor);

		uint8_t res = string[0] * string[1] * 2 * string[2] * 3 * string[3] * 4 * last * 5;
		if(res == 16 && LL(0) && LL(1) && LL(2) && LL(3) && last >= 0x30 && last < 0x80 /*res && res < best*/) {
			printf("%hhu -> %2hhx %2hhx %2hhx %2hhx %2hhx\n", res, string[0], string[1], string[2], string[3], last);
			best = res;


			if(res == 1)
				return 0;
		}
	}	
}
