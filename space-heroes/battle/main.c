#include <stdio.h>
#include <stdint.h>
#include <string.h>

void substract_i_7(char* attack) {
    for(int i = 0; i < strlen(attack); i++) {
        attack[i] += 7 + i; 
    }
}

void add_i(char* attack) {
    for(int i = 0; i < strlen(attack); i++) {
        attack[i] -= i;
    }
}

void xor_i(char* attack) {
    for(int i = 0; i < strlen(attack); i++) {
        attack[i] ^= i;
    }
}

int TurnAction(int round)

{
  int res;
  long success;

  union {
    struct {
  unsigned int local_13;
  unsigned short local_f;
  unsigned char local_d;
    } a;
    char attack[9]; 
  } a;
  char unused;
  
  unused = 0x5f;
  switch(round) {
  default:
    success = 0;
    break;
  case 1:
    a.a.local_13 = 0x6b5a613c;
    a.a.local_f = 0x5e60;
    a.a.local_d = 0;

    xor_i(a.attack);
    xor_i(a.attack);
    add_i(a.attack);
    substract_i_7(a.attack);

    printf("%s\n",a.attack);
    break;
  case 2:
    a.a.local_13 = 0x6e5a653b;
    a.a.local_f = 0x75;
    
    substract_i_7(a.attack);
    add_i(a.attack);
    xor_i(a.attack);
    add_i(a.attack);

    printf("%s\n",a.attack);
    break;
  case 3:
    a.a.local_13 = 0x57525f36;
    a.a.local_f = 0x57;

    substract_i_7(a.attack);
    add_i(a.attack);
    xor_i(a.attack);
    substract_i_7(a.attack);

    printf("%s\n",a.attack);
    break;
  case 4:
    a.a.local_13 = 0x59516144;
    a.a.local_f = 0x6457;
    a.a.local_d = 0;

    xor_i(a.attack);
    substract_i_7(a.attack);
    add_i(a.attack);
    substract_i_7(a.attack);

    printf("%s\n",a.attack);
    break;
  case 5:
    a.a.local_13 = 0x66515f34;
    a.a.local_f = 0x62;

    xor_i(a.attack);
    substract_i_7(a.attack);
    add_i(a.attack);
    add_i(a.attack);
    substract_i_7(a.attack);

    printf("%s\n",a.attack);
    break;
  case 6:
    a.a.local_13 = 0x6b5a613c;
    a.a.local_f = 0x5e60;
    a.a.local_d = 0;

    xor_i(a.attack);
    xor_i(a.attack);
    add_i(a.attack);
    substract_i_7(a.attack);

    printf("%s\n",a.attack);
    break;
  case 7:
    a.a.local_13 = 0x6b766b46;
    a.a.local_f = 0x31;

    add_i(a.attack);
    xor_i(a.attack);
    add_i(a.attack);
    add_i(a.attack);

    printf("%s\n",a.attack);
  }
  return success;
}

int main() {
    for(int i = 1; i <= 7; i++) {
        TurnAction(i);  
    }
}
