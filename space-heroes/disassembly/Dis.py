a = input("Enter the super secret password:")
b = ''
c = 0

for x in a:
    add_x = True

    if x == 'a':
        b += '@'
    elif x == '@':
        b += 'a'
        add_x = False
    elif x == 'o':
        b += '0'
        add_x = False
    elif x == '0':
        b += 'o'
        add_x = False
    elif x == 'e':
        b += '3'
        add_x = False
    elif x == '3':
        b += 'e'
        add_x = False
    elif x == 'l':
        add_x = False

    if add_x:
        b += x
    c += 1

d = 'S0th3combination1sonetw0thr3efourf1ve'

if c % 4 == 0:
    if b == d:
        print('You got the flag!')
        exit(0)
    else:
        print('Oops, try again.')
else:
    print('Oops, try again.')
