#include <openssl/sha.h>
#include <stdio.h>
#include <stdlib.h>

static const unsigned char magic[] = { 
    0xb2, 0xea, 0xe5, 0xbf, 0xbb, 0x8c, 
    0xc6, 0x01, 0x38, 0x72, 0x0b, 0x2f, 
    0x0f, 0x54, 0x9c, 0x6e, 0x40, 0x24, 
    0xea, 0x84, 0xc7, 0xe1, 0x7d, 0x34, 
    0x58, 0xbd, 0x2e, 0xe2, 0xb4, 0x12, 
    0x48, 0xfe 
};

static unsigned char buf[256];

void print_flag() {
    printf("shctf{");
    for(int i = 0; i < 0x20; i++) {
        buf[i] = buf[i] ^ magic[i];
    }
    printf("%32s}\n", buf);
}

int main() {
    int len = 1;

    srand(0x2454);

    int x, y;

    SHA256_CTX ctx;
    SHA256_Init(&ctx);

    long local_20 = 0;

    while(1) {
        x = rand() % 0x21;
        y = rand() % 0x14;

        if(len == 0x295) {
            print_flag();
            return 0;         
        }

        local_20 = x * 0x10 + y; 

        SHA256_Update(&ctx, &local_20, 8);
        SHA256_Final(buf, &ctx);

        len++;
    }
}
