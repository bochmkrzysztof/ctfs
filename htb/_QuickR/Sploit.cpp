#include <stdio.h>


void dropUntil(char ch) {
	while(getchar() - ch)
		;
}

int main() {
	for(int i = 0; i < 9; i ++)
		dropUntil(0x1b);

	char ch = 0;

	while(1) {
		dropUntil(0x1b);

		getchar();
		ch = getchar();

		dropUntil('m');
	
		while(getchar() == ' ') {
			putchar(ch);
		}
	}
}