#include <cctype>
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#include <stdio.h>
#include <string.h>
#include <stdint.h>

const char* Morse[] = {".-", "-...", "-.-.", "-..",".", "..-.", "--.", "....", "..", ".---",
                   "-.-", ".-..", "--", "-.", "---", ".--.", "--.-", ".-.", "...", "-",
                   "..-", "...-", ".--", "-..-", "-.--", "--..", ".----", "..---", "...--",
                   "....-", ".....", "-....", "--...", "---..", "----.", "-----", ".-.-.-",
                   "--..--", "..--..", 0};
const char* Letter[] = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j",
                    "k", "l", "m", "n", "o", "p", "q", "r", "s", "t",
                    "u", "v", "w", "x", "y", "z", "1", "2", "3", "4",
                    "5", "6", "7", "8", "9", "0", "Stop", ",", "?", 0};

int main() {
	int x, y, c;

	stbi_uc* data = stbi_load("pwd.png", &x, &y, &c, 4);

	stbi_uc base[4];
	memcpy(base, data, 4);

	uint32_t l = 0;
	char string[4096];

	for(uint32_t i = 1; i < y; i += 2) {
		if(i >= y) return 0;

		uint32_t k = 0;
		char morse[32];
		morse[0] = 0;

		for(uint32_t j = 1; j < x; j+= 2) {
			if(memcmp(base, &(data[i * 4 * x + j * 4]), 4) == 0) 
				break;

			if(memcmp(base, &(data[i * 4 * x + j * 4 + 4]), 4)) {
				j += 2;
				morse[k++] = '-'; // dash
				morse[k] = 0;
			} else {
				morse[k++] = '.'; // dot
				morse[k] = 0;
			}
		}

		for(uint32_t j = 0; Morse[j]; j++) {
			if(strcmp(morse, Morse[j]) == 0) {
				string[l++] = Letter[j][0];
				string[l] = 0;
				break;
			}
		}
	}

	puts(string);
}