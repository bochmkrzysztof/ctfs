#!/bin/bash

for i in {999..1}
do
	echo -n "$i - "
	P=$(./a.out)
	echo $P
	7z x -p$P "flag_$i.zip" > /dev/null || exit
	rm pwd.png
	rm "flag_$i.zip"

	cp flag/* .
	rm -r flag
done