#include <stdio.h>
#include <stdint.h>

char is_prime(uint64_t a) {
	if(a == 1) return 0;
	for(uint64_t i = 2; i < a - 1; i++) {
		if(!(a % i)) return 0;
	}
	return 1;
}

uint64_t next(uint64_t a) {
  int j; // [rsp+Ch] [rbp-1Ch]
  uint64_t v4; // [rsp+10h] [rbp-18h]
  int64_t v5; // [rsp+18h] [rbp-10h]
  int i; // [rsp+24h] [rbp-4h]
	
  for ( i = 0; i <= 7; ++i )
  {
    v5 = 0LL;
    v4 = a;
    for ( j = 0; j <= 7; ++j )
    {
      if ( (unsigned int)is_prime((unsigned int)(j + 1)) )
        v5 ^= v4 & 1;
      v4 >>= 1;
    }
    a = (v5 << 7) + (a >> 1);
  }
  return a;
}

int main() {
	printf("%llu\n", next(10));
	printf("%llu\n", next(20));
	printf("%llu\n", next(30));
	printf("%llu\n", next(40));
	printf("%llu\n", next(50));
	printf("%llu\n", next(60));
	printf("%llu\n", next(70));
	printf("%llu\n", next(80));
}
