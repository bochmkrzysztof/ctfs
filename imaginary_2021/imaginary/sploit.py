#!/bin/python

from pwn import *
import subprocess

#io = process('./imaginary.py')
io = remote('chal.imaginaryctf.org', 42015)

log.info(io.recvuntil('so watch out!)\n\n'))

p = log.progress("Solved")
l = log.progress("Line")


for i in range(300):
    p.status(str(i) + '/300')

    line = io.recvline()
    l.status(line)
    io.recvuntil('>')

    if line.startswith(b'That'):
        exit(1)
    elif line.startswith(b'__'):
        log.info('Bad line!')
        io.sendline()
    else:
        x = subprocess.run(['./a.out', line], capture_output=True)
        a = x.stdout

        log.info(a)

        io.send(a)

    io.recvuntil('Correct!\n')


io.interactive()
