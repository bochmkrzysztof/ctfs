#include <stdio.h>

int main(int, char** argv) {
	char* str = argv[1];

	int im = 0, re = 0;

	char operator = '+';
	char hasinput = 1;
	
	while(hasinput) {
		str++; // (

		char rs = '+';
		int r = 0;

		if(*str == '-') {
			rs = '-';
			str++;
		}

		while(*str >= '0' && *str <= '9') {
			r *= 10;
			r += *str - '0';
			str++;
		}

		if(rs == '-') r = -r;

		char is = *str++;
		int i = 0;

		while(*str >= '0' && *str <= '9') {
			i *= 10;
			i += *str - '0';
			str++;
		}

		if(is == '-') i = -i;

		if(operator == '-') {
			im -= i;
			re -= r;
		} else {
			im += i;
			re += r;
		}

		str++; // i
		str++; // )

		str++;
		
		if(!*str) break;
		
		operator = *str++;
		str++;
	}

	printf("%d%+di\n", re, im);
}
