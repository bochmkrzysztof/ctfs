#!/bin/python

from pwn import *

#io = process('./maze.py')
io = remote('chal.imaginaryctf.org', 42017)

solver = process('./a.out')

p = log.progress('Progress')

p.status("Generating maze")
log.info(io.recvuntil('This is your maze:\n'))

p.status('Receiving maze')
maze = io.recvuntil('F\n\n\n\n')

p.status('Solving maze')
solver.send(maze)

p.status('Solved maze')
move = solver.recvline()
log.info("Move: " + str(move))

p.status('Waiting to send move')
io.recvuntil('Enter your move:')

p.status('Performing move')
io.send(move)

io.interactive()
