#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <queue>

struct point {
	char a,b,c,d;
	int distance;
	char from;
};

#define MAZE maze[d + 10*c + 100*b + 1000*a]
#define MOVES moves[d + 10*c + 100*b + 1000*a]

int main() {
	char* maze  = new char[10000];
	char* moves = new char[10000]; 

	auto comparator = [](const point& p1, const point& p2) {
		return p1.distance > p2.distance;	
	};

	std::priority_queue<point, std::vector<point>, decltype(comparator)> pq(comparator);

	memset(moves, '.', 10000);

	moves[0] = '@';

	for(int a = 0; a < 10; a++) {
		for(int b = 0; b < 10; b++) {
			for(int c = 0; c < 10; c++) {
				for(int d = 0; d < 10; d++) {
					char ch = getchar();
					if(ch == '@' || ch == 'F') ch = '.';
					maze[d + 10*c + 100*b + 1000*a] = ch;
				}
				getchar();
			}
			getchar();
		}
		getchar();
	}

	pq.push({0, 0, 0, 1, 1, 'D'});
	pq.push({0, 0, 0, 9, 1, 'd'});
	pq.push({0, 0, 1, 0, 1, 'C'});
	pq.push({0, 0, 9, 0, 1, 'c'});
	pq.push({0, 1, 0, 0, 1, 'B'});
	pq.push({0, 9, 0, 0, 1, 'b'});
	pq.push({1, 0, 0, 0, 1, 'A'});
	pq.push({9, 0, 0, 0, 1, 'a'});

	while(1) {
		point p = pq.top();
		pq.pop();

		char a = p.a, b = p.b, c = p.c, d = p.d;

		if(p.a == 9 && p.b == 9 && p.c == 9 && p.d == 9) {
			MOVES = p.from;
		
			char* path = new char[p.distance + 1];
			path[p.distance] = 0;

			int ptr = p.distance - 1;
			do {
				path[ptr] = MOVES;
				switch(path[ptr]) {
					case 'A':
						a--;
						if(a < 0) a = 9;
						break;
					case 'a':
						a++;
						if(a > 9) a = 0;
						break;
					case 'B':
						b--;
						if(b < 0) b = 9;
						break;
					case 'b':
						b++;
						if(b > 9) b = 0;
						break;
					case 'C':
						c--;
						if(c < 0) c = 9;
						break;
					case 'c':
						c++;
						if(c > 9) c = 0;
						break;
					case 'D':
						d--;
						if(d < 0) d = 9;
						break;
					case 'd':
						d++;
						if(d > 9) d = 0;
						break;
				};
			} while(ptr--);

			puts(path);
			puts("");
			puts("");
			puts("");
			puts("");
			puts("");
			puts("");
			puts("");
			puts("");
			puts("");
			puts("");

			return 0;
		}
		if(MAZE == '#') continue;
		if(MOVES != '.') continue;

		MOVES = p.from;

		pq.push({a, b, c, d + 1 < 10 ? d + 1 : 0, p.distance+1, 'D'});
		pq.push({a, b, c, d - 1 >= 0 ? d - 1 : 9, p.distance+1, 'd'});
		pq.push({a, b, c + 1 < 10 ? c + 1 : 0, d, p.distance+1, 'C'});
		pq.push({a, b, c - 1 >= 0 ? c - 1 : 9, d, p.distance+1, 'c'});
		pq.push({a, b + 1 < 10 ? b + 1 : 0, c, d, p.distance+1, 'B'});
		pq.push({a, b - 1 >= 0 ? b - 1 : 9, c, d, p.distance+1, 'b'});
		pq.push({a + 1 < 10 ? a + 1 : 0, b, c, d, p.distance+1, 'A'});
		pq.push({a - 1 >= 0 ? a - 1 : 9, b, c, d, p.distance+1, 'a'});
			
	}
}
