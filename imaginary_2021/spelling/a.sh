#!/bin/bash

while read p; do
	grep $p lowercase.txt > /dev/null
	if [ $? -eq 0 ] 
	then
		echo -n
	else
		echo $p
	fi
done < chal.txt
