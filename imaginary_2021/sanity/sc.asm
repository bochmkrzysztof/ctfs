[bits 64]
section .text
global _start

search_data_start:
	mov rbx, rax
	add rbx, rdi

.loop:
	cmp rax, rbx
	je .fail

	cmp byte [rax+0],13
	jne .next
	cmp byte [rax+1],10
	jne .next
	cmp byte [rax+2],13
	jne .next
	cmp byte [rax+3],10
	je .found

.next:
	inc rax
	jmp .loop

.found:
	add rax, 8
	ret

.fail:
	xor rax, rax
	ret



_start:
	mov rax, 41			; socket
	mov rdi, 2			; AF_INET
	mov rsi, 1			; SOCK_STREAM
	xor rdx, rdx
	syscall

	mov r15, rax

	mov rdi, rax
	mov rax, 42			; connect
	lea rsi, [server]
	mov rdx, serverlen
	syscall

	mov rax, 1			; write
	mov rdi, r15
	lea rsi, [req1]
	mov rdx, req1len
	syscall
	
	xor rax, rax			; read
	mov rdi, r15
	lea rsi, [input]
	mov rdx, inputlen
	syscall

	mov rdi, rax
	lea rax, [input]
	call search_data_start
	
	test rax, rax
	je .fail



	xor rdi, rdi
	call exit

.fail:
	mov rdi, 1
	call exit




exit:
	mov rax, 60
	syscall

section .data
server:	db 0x02,0x00,0x00,0x50, 157, 230,   1,  41,0xa0,0x50,0x55,0x55,0x55,0x55,0x00,0x00
serverlen equ $ - server

req1:	db "GET /api/challenges/archived HTTP/1.1",13,10
	db "Host: 2021.imaginaryctf.org",13,10
	db "User-Agent: curl/7.78.0",13,10
	db "Accept: */*",13,10
	db 13,10
req1len equ $ - req1




section .bss
input:	resb 65536 
inputlen equ $ - input
