#include <stdio.h>
#include <stdint.h>

int main() {
	int c;
	for(uint32_t i = 0; (c = getchar()) != EOF; i++) {
		if(i & 1) {
			putchar(c + i);
		} else {
			putchar(c - i);
		}
	}
}
