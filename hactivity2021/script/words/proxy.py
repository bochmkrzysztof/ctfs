#!/bin/python

from pwn import *

io = remote('challenge.ctf.games', 31925)

p = log.progress('Progress')

io.recvuntil('> ')
io.sendline('play')

sides = [
        [1, 0],
        [-1,0],
        [0, 1],
        [0,-1],
        [1, 1],
        [-1,1],
        [1,-1],
        [-1,-1]
        ]

class fin(Exception): pass

for b in range(30):
    p.status('' + str(b) + '/30')
    io.recvuntil('---\n')

    tabble = ''

    for y in range(16):
        io.recvuntil('|')
        for x in range(16):
            letter = io.recv(4)[3]
            tabble += chr(letter)


        
    io.recvuntil('Y ')
    io.recvline()

    for a in range(5):
        word = io.recvuntil('> ')[0:-4].decode('ascii')

        log.info(str(a) + ' ' + str(b) + ': ' + word)
        
        try:
            for y in range(16):
                for x in range(16):
                    for i in range(8):
                        dx = sides[i][0]
                        dy = sides[i][1]

                        X = x
                        Y = y

                        _x = x
                        _y = y

                        bad = False
                        coords = []

                        for j in range(len(word)):
                            if tabble[16 * Y + X] != word[j]:
                                bad = True
                                break
                            elif not bad:
                                coords += [(_x, _y)]

                            X += dx
                            Y += dy
                            _x += dx
                            _y += dy

                            if X >= 16:
                                X -= 16
                            elif X < 0:
                                X += 16
                            if Y >= 16:
                                Y -= 16
                            elif Y < 0:
                                Y += 16

                        if not bad:
                            io.sendline(str(coords))
                            log.info(str(coords))                    
                            raise fin
        except fin:
            pass

p.success('done')
io.interactive()
