#include <stdio.h>

int main() {
	char in[4096];
	fgets(in, 4096, stdin);

	char* iptr = in;
	while(*iptr != 10) {
		char a = iptr[0] - 0x30;
		char b = iptr[1] - 0x30;
		char c = iptr[2] - 0x30;

		int byte = a * 64 + b * 8 + c;
		putchar(byte);

		iptr += 3;
	}
}
