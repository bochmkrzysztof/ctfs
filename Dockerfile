FROM archlinux

RUN pacman -Syu --noconfirm
RUN pacman -S --noconfirm gcc gdb vim git wget which curl python python-pip
RUN pip install pwnlib

WORKDIR /root

RUN bash -c "$(curl -fsSL https://gef.blah.cat/sh)"
